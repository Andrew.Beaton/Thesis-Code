 # -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 18:55:18 2021

@author: Andrew
"""

import os 
import pandas as pd
from sys import exit
import matplotlib.pyplot as plt 
import seaborn as sns
plt.rcParams['font.sans-serif'] = "DejaVu Sans"

dir_path = "Z:\\Andrew\\E210-Data\\Final Bulk data\\05mj"
dir_path_1mj ="Z:\\Andrew\\E210-Data\\Final Bulk data\\1mj"
dir_path_5mj ="Z:\\Andrew\\E210-Data\\Final Bulk data\\5mj"


column_map = ["index","dump","charge","Emean","derms","x_mean","y_mean","z_mean","deperc","length_max","yrms",
              "zrms","div_y_rms","div_z_rms","emit_y_rms","emit_z_rms","peak_curr",
              "fd_bright","timing"]

def get_axis_limits(ax, scale=.9):
    return ax.get_xlim()[1]*scale, ax.get_ylim()[1]*scale

def emittance_plot(data,color_var,used_map,save_var,savename,titleadd):
    fig, axs  =  plt.subplots(3,1,constrained_layout=True,figsize =[7,8])
    plot=axs[0].scatter(data.timing,data.charge,c=color_var,cmap = used_map)
    Ecrossing = 0.1832
    axs[0].axvline(Ecrossing,alpha =0.65)
    title = "Charge and Emittance of "+ titleadd
    fig.suptitle(title)
    
    
    cbar = fig.colorbar(plot,ax=axs)
    axs[1].scatter(data.timing,data.emit_y_rms,c=color_var,cmap =used_map,label ="y",marker="D")
    axs[1].scatter(data.timing,data.emit_z_rms,c=color_var,cmap =used_map,marker="s",label ="z")
    axs[1].legend(loc ="right")
    axs[1].axvline(Ecrossing,alpha =0.65)
    
    axs[1].ticklabel_format(axis="y",style="sci",scilimits=(0,0))
    axs[2].ticklabel_format(axis="y",style="sci",scilimits=(0,0))
    #Untrimmed Emittance

    axs[2].scatter(data.timing,data.emit_z_rms,c=color_var,cmap = used_map,marker="s")
    axs[2].scatter(data.timing,data.emit_y_rms,c=color_var,cmap = used_map,marker="D")
    axs[2].axvline(Ecrossing,alpha =0.65)
    axs[2].set_ylim([0,6e-6])
    #Labeling
    cbar.ax.set(ylabel ='5D Brightness (A$\mathregular{m^{-2}rad^{-2}}$)')
    axs[0].set_ylabel("Injected Charge (pC)")
    axs[1].set_ylabel("Emittance (m rad)")
    axs[2].set_ylabel("Emittance (m rad)")
    axs[2].set_xlabel("Relative timing (ps)")

    #Label the subplots
    axs[0].annotate('A', xy=get_axis_limits(axs[0]),size=15)
    axs[1].annotate('B', xy=get_axis_limits(axs[1]),size=15)
    axs[2].annotate('C', xy=get_axis_limits(axs[2]),size=15)
    
    
    
    if save_var == True :
        plt.savefig(savename,dpi = 300)
        print(savename+" saved !" )

def loadfiles_e210 (directory):
    data_timing_prelim = []
    data = []
    for filename in os.listdir(directory):
      if filename.endswith(".txt") or filename.endswith(".py"): 
          try:
             file_location = (os.path.join(directory, filename))
             df = pd.read_csv(file_location,index_col=None, header = 0)
          except EOFError as ex:
              print("File error: check dir_path for errors")
              raise ex
              
              
              
          data_timing_prelim.append(filename[:-4])
          data.append(df)
          continue
      else:
          continue
    return data,data_timing_prelim


def e210_cleaner (frame_data):
    frame_data.dropna(axis=0,inplace=True)
    
    frame_data.reset_index(inplace=True)
    frame_data = frame_data.apply(pd.to_numeric, errors="coerce")
    frame_data = frame_data.loc[:,(frame_data != 0).any(axis=0)]
    frame_data.reset_index()#inplace=True)
    return frame_data


def completeLoad(dir_path):
    Data_file =loadfiles_e210(dir_path)
    
    data_electrons = Data_file[0]
    
    data_timing_prelim = Data_file[1]
    
    data_electrons= pd.concat(data_electrons, axis=0,ignore_index=True)
    
    data_electrons = e210_cleaner(data_electrons)
    
    timing_data = pd.Series(data_timing_prelim,name="timing")
    
    final_electron_data= pd.concat([data_electrons,timing_data],ignore_index = False,axis=1)
    #return final_electron_data
    final_electron_data.columns = column_map
    return final_electron_data

def fix_timing(data):
    for index,row in data.iterrows():
       #adds a decimal to the start of the timing scale  
       data.iloc[index,-1] = ('.' + data.iloc[index,-1])
    data['timing'] = data['timing'].astype(float).round(3)
    return data


def data_saver(data,name):
    savename = "E210_" +name+".pkl"
    data.to_pickle(savename)

# frame_data.columns = column_map
mj05 = completeLoad(dir_path)
mj1 = completeLoad(dir_path_1mj)
mj5 = completeLoad(dir_path_5mj)
#Data_file =loadfiles_e210(dir_path_1mj)




mj05=fix_timing(mj05)
mj1=fix_timing(mj1)
mj5= fix_timing(mj5)




mj05.sort_values(by=["timing"])

# mj05.plot.scatter(x="timing",y="charge",title="05mj Charge scatter")
# mj1.plot.scatter(x="timing",y="charge",title="1mj Charge scatter")
# mj5.plot.scatter(x="timing",y="charge",title="5mj Charge scatter")
#frame_data.pl["Emean","charge"])

#Seaborn Testing
# fig, axs  =  plt.subplots(1,1)
# sns.scatterplot(data=mj05,x="timing",y="charge",size="peak_curr",hue="5d_bright",legend=False,ax=axs)#
# sns.scatterplot(data=mj1,x="timing",y="charge",size="peak_curr",hue="5d_bright",legend=False,ax=axs)
# sns.scatterplot(data=mj5,x="timing",y="charge",size="peak_curr",hue="5d_bright",legend=False,ax=axs)


used_map = "plasma"
color_var = mj05.fd_bright

#data_saver(mj05,"mj05")**
#data_saver(mj1,"mj1")
#data_saver(mj5,"mj5")
#print(mj05.loc[mj05.groupby("fd_bright")["timing"].max())
#print(mj05["timing"].mj05["fd_bright"].max())

"""
emittance_plot(mj05,mj05.fd_bright,used_map,True,"05mj_BeamQuality","0.5mj Injection Laser")
emittance_plot(mj1,mj1.fd_bright,used_map,True,"1mj_BeamQuality","1mj Injection laser")
emittance_plot(mj5,mj5.fd_bright,used_map,True,"5mj_BeamQuality","5mj Injection laser")
"""

