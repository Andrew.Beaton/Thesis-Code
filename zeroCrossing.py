# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 21:10:59 2021

@author: andrewb
"""
import h5py
import thesis_functions as tf
import pandas as pd 
import numpy as np 
import matplotlib.pyplot  as plt

filelocation = "Z:\\Andrew\\E210-Data\\5mj\\20180303_TrojanV4_1_DumpPer_5mJ_0ps\\20180303_TrojanV4_1_DumpPer_5mJ_0ps_ElecMultiField_225.h5"
data = np.array( tf.loadh5_field(filelocation,"ElecMultiField") )

y_slice =  data[:,84,84,0]

plt.plot(y_slice)