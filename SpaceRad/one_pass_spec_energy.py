# -*- coding: utf-8 -*-
"""
Created on Fri Jul 23 11:19:55 2021

@author: Andrew
"""
import uproot as ur
import pandas as pd 
import pickle
import gc
class spectrum():
    
    def __init__(self):
        self.spectrum = pd.DataFrame(columns =["k"])
        self.k_total =0
        self.count = 0
    
    def primary_spec(self,batch_data):
        """Extracts primary particles in their first step for analysis
        extracts the energy of selected particles"""
        
        self.l_data = batch_data["k"][(batch_data["trk"] == 1)&
                                 (batch_data["subentry"] == 0)]
        return self.l_data
        
    def particle_counter(self):
        self.count = self.count + self.l_data.shape[0]
        
    def spectrum_combiner(self):
        """This creates a collection of all selected particles for each file"""
        frames =[self.spectrum,self.l_data]        
        self.spectrum = pd.concat(frames)
        
    def energy_sum(self):
        self.k_total = self.k_total+ self.l_data.sum()*1e3*1.60218e-19
        
    
    def save_spec(self,path):
        """Saves data from spectrum combiner """
        a_file = open(path+"Spectrum_of_primaries"
                      +"_.pkl","wb")
        pickle.dump(self.spectrum,a_file)
        a_file.close()
        
    def dump_data(self, path):
        with  open(path+"Primary_spec"+
                   "_.txt","w") as f :
            f.write("Primary Spec k total:" + str(self.k_total))
            f.write('\n')
            f.write("Count Of particles:" + str(self.count))

            
def run (file):
    root_loc = file_path_dicts[file]+file_name_dicts[file]
    root_file = ur.open(root_loc)
    root_file = root_file["t"]
    print("Starting file " + file_name_dicts[file] )
    
    primaries = spectrum()
    for batch, report in root_file.iterate(step_size="5000kb", report=True,library="pd"):
            dat_batch = batch[0].reset_index()
            last =  primaries.primary_spec(dat_batch)
            primaries.spectrum_combiner()
            primaries.energy_sum()
            primaries.particle_counter()
            gc.collect()
            
    primaries.save_spec(file_path_dicts[file])
    primaries.dump_data(file_path_dicts[file])
    return last


########################
#File locations and names
########################
file_path_dicts = dict()

file_name_dicts=dict ()
        
f1_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\Reference Spectrums\\upper_belt\\reference_protons\\"
f1_name = "LowerBelt_Proton_1l.root"
file_path_dicts["f1"] = f1_path
file_name_dicts["f1"] = f1_name


extract = run("f1")