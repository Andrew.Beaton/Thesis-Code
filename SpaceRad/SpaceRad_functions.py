
import pickle as pk
import pandas as pd
import matplotlib.pyplot as plt
import gc
def setfonts():
    
    plt.rcParams['font.family'] = 'DejaVu Sans'
    plt.rcParams['mathtext.fontset'] = 'custom'
    plt.rcParams['font.sans-serif'] = "DejaVu Sans"
    plt.rcParams['mathtext.cal'] = 'DejaVu Sans'
    plt.rcParams['mathtext.it'] = 'DejaVu Sans:italic'
    plt.rcParams['mathtext.rm'] = 'DejaVu Sans'
    plt.rcParams['text.usetex'] = False

def load_data(path):
    f = open(path,"rb") 
    data =  pk.load(f)
    f.close()
    return data

def sum_energy(data):
    return data["k"][data["subentry"]==0].sum()

def plot_spec_hist(data):
    plt.hist(data, bins = 500)
    plt.xlabel('Particle Energy (MeV)')
    plt.ylabel('Particle Count')
    plt.title('Particle Energy Spectrum')
    plt.yscale("log")
    plt.show()

def plot_momspec_hist(data):
    plt.hist(data, bins = 500)
    plt.xlabel('Particle Momentum (MeV)')
    plt.ylabel('Particle Count')
    plt.title('Particle Momentum Spectrum')
    plt.yscale("log")
    plt.show()



def energydepvols(data):
    res = dict()
    try:
        ed_vol1 = (data[1]["de"].sum()*1e3)*1.60218e-19
        res[1] = ed_vol1
    except:
        print("Issue with summing volume 1 ")
    try:
        ed_vol2 = (data[2]["de"].sum()*1e3)*1.60218e-19
        res[2] = ed_vol2
    except:
        print("Issue with summing volume 2 ")
        
    try:
        ed_vol3 = (data[3]["de"].sum()*1e3)*1.60218e-19
        res[3] = ed_vol3
    except:
        print("Issue with summing volume 3 ")
        
    try:
        ed_vol4 = (data[4]["de"].sum()*1e3)*1.60218e-19
        res[4] = ed_vol4
    except:
        print("Issue with summing volume 4 ")
        
    try:
        ed_vol5 = (data[5]["de"].sum()*1e3)*1.6e-19
        res[5] = ed_vol5
    except:
        print("Issue with summing volume 5 ")
    gc.collect()
    return res 

def selectronon_from_dict(data,pdg_select):
    """ Intakes dict and searches for intiail instance of requrired pdg"""
    r_data = dict()
    for key in data:
            loop_data =data[key]
            r_data[key] = loop_data[(loop_data["pdg"] == pdg_select) & (loop_data["stp"]==0)]
    return r_data

def selectronon_all_dedup(data,pdg_select):
    """ take in dict and earched for matches to pdg and removes duplicates
    based on enetry and trk"""
    r_data = dict()
    for key in data:
            loop_data =data[key]
            loop_data.sort_values(["entry","subentry"], inplace = True)
            r_data[key] = loop_data[(loop_data["pdg"] == pdg_select)
                                    ].drop_duplicates(subset=["entry","trk"])
    return r_data


def selectronon_from_dict_neq(data,pdg_select):
    """ Intakes dict and searches for intiail instance that is not the pdg"""
    r_data = dict()
    for key in data:
            loop_data =data[key]
            r_data[key] = loop_data[(loop_data["pdg"] != pdg_select) & (loop_data["stp"]==0)]
    return r_data

def trk_1up(data):
    """ Intakes dict and returns particles with trk >1
    for each data set in the dict  duplicated removed"""
    r_data = dict()
    for key in data:
            loop_data =data[key]
            r_data[key] = loop_data[loop_data["trk"] > 1
                                    ].drop_duplicates(subset=["entry","trk"])
    return r_data

def secondary_finder(data):
    """ finds all secondarys produced"""
    r_data = dict()
    for key in data:
            loop_data =data[key]
            r_data[key] = loop_data[(loop_data["trk"] > 1) & (loop_data["stp"]==0)]
    return r_data

def type_particle_counter(data):
    """ Intakes dict and searches for intiail instance that is not the pdg"""
    r_data = dict()
    for key in data:
            loop_data =data[key]
            r_data[key] = loop_data["pdg"].value_counts()
    return r_data

def gamma_mins(data):
    """ Intakes dict and returns the minimum k  >0 for each data set in the dict """
    r_data = dict()
    for key in data:
            loop_data =data[key]
            r_data[key] = loop_data["k"][loop_data["k"]> 0].min()
    return r_data

def gamma_max(data):
    """ Intakes dict and returns the max k for each data set in the dict """
    r_data = dict()
    for key in data:
            loop_data =data[key]
            r_data[key] = loop_data["k"].max()
    return r_data