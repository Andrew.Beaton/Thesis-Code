# -*- coding: utf-8 -*-
"""
Created on Sun Nov 21 11:37:22 2021

@author: andrewb
"""

import SpaceRad_functions as spr
import gc
import matplotlib.pyplot as plt 
import matplotlib as mpl
import pandas as pd
mpl.rcParams['agg.path.chunksize'] = 10000000


spr.setfonts()

path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ed_reference\\Lower_Belt\\Reference_gamma_boosted\\gamma_electrons\\g1\\spectrum_of_22_in_2_.pkl"
path2 ="Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ed_reference\\Lower_Belt\\Reference_gamma_boosted\\gamma_electrons\\g2\\spectrum_of_22_in_2_.pkl"

file_data = spr.load_data(path)
file2_data = spr.load_data(path2)
#Combine data
comb_data = pd.concat([file_data,file2_data],axis = 0).reset_index()
#converts into MeV
mev_spec = comb_data[0]/1e3 

ppt = mev_spec.plot.hist( bins= 5000,logx =True, logy = True,
              legend = False,color="royalblue" ) 
ppt.set_title("Lower Belt Electrons Cobalt-60 Equivalent On Target Photon Spectrum")
ppt.set_xlabel("Energy (MeV)")



plt.savefig("Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ed_reference\\Lower_Belt\\Reference_gamma_boosted\\gamma_electrons\\g1\\Lower_Electrons_REF_Target_spec_Gamma_Photon.png", dpi = 400)

plt.show()