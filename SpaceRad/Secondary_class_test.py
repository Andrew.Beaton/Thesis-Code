# -*- coding: utf-8 -*-
"""
Created on Fri Jul  9 21:06:09 2021

@author: Andrew
"""
import pickle as pk 
import SpaceRad_functions as srf
import matplotlib.pyplot as plt
import numpy as np 

class primary_check():
    
    def __init__(self, path, file):
        self.path = path 
        self.file = file 
    
    def load_data(self):
        self.data = srf.load_data(self.path+self.file)
    
    def filter_p (self):
        target =  self.data[2]
        self.data = target[target["pdg"]== 2212]
        #["entry","subentry","k","pdg"]
        
    def return_data(self):
        return self.data
        
class secondary:
    def __init__(self, path, file):
        self.path = path 
        self.file = file
        
    def load_data(self):
        #function loads data from pickle 
        f = open(self.path+self.file,"rb") 
        self.data =  pk.load(f)
        f.close()
    
    def data_length(self):
        print(type(self.data))
    
    def return_data(self):
        return self.data
        


class secondary_detail:
    def __init__(self, path, file):
        self.path = path 
        self.file = file
        
    def load_data(self):
        """Calls loading function stores at class object.data"""
        self.data = srf.load_data(self.path+self.file)
        
    def extract_secondaries(self):
        return srf.secondary_finder(self.data)
        
    def secondary_photons(self):
        """Finds all nphotons particles"""
        self.photons = srf.selectronon_from_dict(self.data,22)
        return self.photons
    
    def secondary_electrons(self):
        """Finds all secondary electrons particles"""
        self.photons = srf.selectronon_from_dict(self.data,11)
        return self.photons
    
    def secondary_antielectrons(self):
        """Finds all secondary electrons particles"""
        self.photons = srf.selectronon_from_dict(self.data,-11)
        return self.photons
    
    def secondary_protons(self):
        """Finds all secondary protons particles"""
        self.photons = srf.selectronon_from_dict(self.data,2212)
        return self.photons
    
    def secondary_al27(self):
        """Finds all secondary al27 particles"""
        self.photons = srf.selectronon_from_dict(self.data,1000130270)
        return self.photons
    
    def secondary_si28(self):
        """Finds all nphotons particles"""
        self.photons = srf.selectronon_from_dict(self.data,1000140280)
        return self.photons
    
    def high_trk(self):
        """Returns all entries with trk> 1"""
        return srf.trk_1up(self.data)

    
    def extract_si28(self):
        """Finds all nphotons particles"""
        return srf.selectronon_from_dict(self.data,1000140280)
        
    
    def not_protons(self):
        """Finds all non proton particles"""
        return srf.selectronon_from_dict_neq(self.data,2212)
    
    def not_electrons(self):
        """Finds all non proton particles"""
        self.not_electrons = srf.selectronon_from_dict_neq(self.data,11)
        return self.not_electrons
    
    def not_photons(self):
        """Finds all non photon particles"""
        self.not_electrons = srf.selectronon_from_dict_neq(self.data,22)
        return self.not_electrons
    
    def type_count(self,data):
        """Counts the number of each type of particle"""
        return srf.type_particle_counter(data)
    
    def extract_photons_all(self):
        """ This returns all photon data for all steps"""
        return srf.selectronon_all_dedup(self.data,22)
    
    def extract_electrons_all(self):
        """ This returns all photon data for all steps"""
        return srf.selectronon_all_dedup(self.data,11)
    
    def extract_anti_electrons_all(self):
        """ This returns all photon data for all steps"""
        return srf.selectronon_all_dedup(self.data,-11)

    
    def data_dump(self):
        return self.data
    
class gamma_master:
    def __init__(self, g1_path, g1_file, g2_path, g2_file):
        
        self.g1_path = g1_path 
        self.g1_file = g1_file
        
        self.g2_path = g2_path 
        self.g2_file = g2_file
        
    def load_data(self):
        """Calls loading function stores at class object.data"""
        self.g1_data = srf.load_data(self.g1_path+self.g1_file)
        self.g2_data = srf.load_data(self.g2_path+self.g2_file)
        
    def max_k_genvol(self):
        """Returns the max photon energy generated in each vlm 
        from g1 and g2"""
        g1_m = srf.gamma_max(self.g1_data)
        g2_m = srf.gamma_max(self.g2_data)
        return [g1_m, g2_m]
    
    def min_k_genvol(self):
        """Returns the min photon energy generated in each vlm
        from g1 and g2
        """
        g1_mn = srf.gamma_mins(self.g1_data)
        g2_mn = srf.gamma_mins(self.g2_data)
        return [g1_mn, g2_mn]

    def get_sec(self):
        """Finds all non photon particles stp = 0 """
        self.g1_sec = srf.secondary_finder(self.g1_data)
        self.g2_sec = srf.secondary_finder(self.g2_data)
    
    def sec_count(self):
        """Counts number of secondary occurances of each type"""
        g1_count = srf.type_particle_counter(self.g1_sec)
        g2_count = srf.type_particle_counter(self.g2_sec)
        return [g1_count,g2_count]
        
    
def photon_spectrum_1(data,title,save_name,save_var = False):
    data.sort_values("k", inplace = True)
    x = np.arange(0,data.shape[0],1)
    fig, ax =plt.subplots()
    ax.scatter(x=x, y=data["k"]/1e3,color ="dodgerblue")
    ax.set_xlabel("Photon Number")
    ax.set_ylabel("Photon Energy (MeV)")
    ax.set_title(title)
    if save_var == True :
        plt.savefig(save_name,dpi = 300)
    plt.show()

path_p ="D:\\Gean4_data\\al_layers_lower\\1l\\Protons\\ol\\"
file_p = "vlm_sec_dict_step.pkl"
p_protons = primary_check(path_p,file_p)
p_protons.load_data()
p_protons.filter_p()
protons = p_protons.return_data()
"""

#mono gamma 
g1_path ="D:\\Gean4_data\\al_layers_lower\\1l\\gamma_electrons\\g1\\"
g2_path ="D:\\Gean4_data\\al_layers_lower\\1l\\gamma_electrons\\g2\\" 

g1_name = "vlm_sec_dict_step.pkl"
g2_name = "vlm_sec_dict_step.pkl"

gammas = gamma_master(g1_path,g1_name,g2_path,g2_name)
gammas.load_data()
gammas.get_sec()
gamma_sec_count = gammas.sec_count()
gamma_min = gammas.min_k_genvol()
gamma_max = gammas.max_k_genvol()

"""
# Detail extraction 

path_detail = "D:\\Gean4_data\\al_layers_lower\\4l\\Electrons\\"
file_detail = "vlm_sec_dict_step.pkl"


"""
#########################
l1_detail = secondary_detail(path_detail, file_detail)
l1_detail.load_data()
all_secondaries_generated = l1_detail.extract_secondaries()
#testdata = l1_detail.data_dump()
photons_generated = l1_detail.secondary_photons()

photons_all = l1_detail.extract_photons_all()

electrons_generated =l1_detail.secondary_electrons()
antielectrons_generated = l1_detail.secondary_antielectrons()
electrons_all = l1_detail.extract_electrons_all()

protons_generated = l1_detail.secondary_protons()
anti_electrons_all = l1_detail.extract_anti_electrons_all()

trk_above_0 = l1_detail.high_trk()
trk_above_0_count = l1_detail.type_count(trk_above_0)

sec_photons_on_target = trk_above_0[2][trk_above_0[2]["pdg"] == 22]
sec_electrons_on_target = trk_above_0[2][trk_above_0[2]["pdg"] == 11]

not_photons = l1_detail.not_photons()

al27 =l1_detail.secondary_al27()
si28 = l1_detail.secondary_si28()
secondary_count = l1_detail.type_count(all_secondaries_generated)
#########################
"""


"""
srf.setfonts()
plt_title =  "Secondary Photon Spectrum Generated at 1st Al layer"
save_name = "Z:\\Andrew\\Thesis_2019\\V11\\figures\\SpaceRad\\LowerBelt_Proton_secondaries_l2_V1.png"
photon_spectrum_1(photons_generated[1],plt_title,save_name,False)

plt_title = "Secondary Photons Recorded on Target 2l Shielding"
save_name = "Z:\\Andrew\\Thesis_2019\\V11\\figures\\SpaceRad\\LowerBelt_Proton_secondaries_OT_l2_V1.png"
photon_spectrum_1(photons_all[2],plt_title,save_name,False)
"""
