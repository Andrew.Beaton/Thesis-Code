# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 13:53:34 2021

@author: Andrew
"""
import SpaceRad_functions as spr
import gc

spr.setfonts()
#path  = "D:\\Gean4_data\\E_p_test\\vlm_final_dict_step.pkl"
path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ed_reference\\Lower_Belt\\Reference_gamma_boosted\\gamma_electrons\\g1\\spectrum_of_11_in_2_.pkl"
#compare_path = "Z:\\Andrew\\Thesis_SpaceRad\\Lower_belt_flux\\Proton_flux.csv"

file_data = spr.load_data(path)
#compare_data = pd.read_csv(compare_path)
#plot_momspec_hist(compare_data)

vol = file_data[0]
del file_data
e_spectrum =vol["k"][(vol["subentry"] == 0)] *1e-3
p_spectrum =vol["p"][vol["subentry"] != 0] *1e-3
non_zero = vol[["subentry","stp","p"]][(vol["subentry"] == 26)&(vol["p"]!=0)]
#non_zero = non_zero.sort_values()
p_spectrum =vol["p"]*1e-3
#non_zero = vol_1["p"][vol_1["p"]!=0]
spr.plot_spec_hist(e_spectrum)
spr.plot_momspec_hist(p_spectrum)


#Memory Control
del e_spectrum,p_spectrum,
gc.collect()



######
# Energy Dep
######
ed_path = "D:\\Gean4_data\\al_layers_lower\\1l\\Electrons\\vlm_eng_dep.pkl"
ed_data =spr.load_data(ed_path)
energy_dep = spr.energydepvols(ed_data)
print ("Code failed sucessfully")
