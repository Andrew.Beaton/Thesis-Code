# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 17:54:41 2021

@author: Andrew
"""

import uproot as ur
import pandas as pd
path = "D:\\Gean4_data\\al_layers_lower\\1l\\Protons\\LowerBeltProt_1l.root"

root_file  = ur.open(path)
root_file  = root_file["t"]

id_particle =  root_file.arrays(["pdg","k"],library = "pd")
id_particle = id_particle.reset_index()
#print ("Proton particle id is ")
primary_p   = id_particle[id_particle["subentry"]==0]
primary_p_zeros   = id_particle[(id_particle["subentry"]==0)&(id_particle["k"]==0)]
primary_p["k"].plot.hist(logy = True,bins = 200)
print (primary_p["k"].describe())
