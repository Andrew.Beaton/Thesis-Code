# -*- coding: utf-8 -*-
"""
Created on Wed Jun 23 17:09:25 2021

@author: Andrew
"""
import pandas as pd 
import matplotlib.pyplot as plt
def setfonts():
    
    plt.rcParams['font.family'] = 'DejaVu Sans'
    plt.rcParams['mathtext.fontset'] = 'custom'
    plt.rcParams['font.sans-serif'] = "DejaVu Sans"
    plt.rcParams['mathtext.cal'] = 'DejaVu Sans'
    plt.rcParams['mathtext.it'] = 'DejaVu Sans:italic'
    plt.rcParams['mathtext.rm'] = 'DejaVu Sans'
    plt.rcParams['text.usetex'] = False
    
setfonts()
path  ="Z:\\Andrew\\Thesis_SpaceRad\\Lower_belt_flux\\Proton_flux.csv "
Outpath = ""
p_data = pd.read_csv(path)
plt.figure(figsize=(8,5),dpi =200)
plt.scatter(p_data["Energy"],p_data["IFlux"],c="royalblue")

plt.yscale("log")
plt.xscale("log")
plt.xlabel("Energy " r"($MeV$)")
plt.ylabel("Integral Flux (" r"$cm^2 s^{-1} $"")" )
plt.title("Protron Energy Spectrum - Lower Van Allen Belt")
plt.savefig(Outpath, format="png")
