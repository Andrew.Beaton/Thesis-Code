# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 20:22:08 2021

@author: Andrew
"""

import uproot as ur
import pandas as pd 
import numpy as np 
import SpaceRad_functions as srf
import pickle
import gc


def data_collie(data,request):
    """
    In = root file array list, list 
    Out =  pandas df 
    """
    #Setting up variables
    r_data = pd.DataFrame()
    #build dictionary for results
    col_dat = dict()
    
    #loops for all requests
    for r in request:
        try:
            col_dat[r] = data[r]
        except:
            print("There was an error in data retrieval "+r)
    r_data = pd.DataFrame.from_dict(col_dat)
    

    return r_data

def vol_count(data):
    """
    In =pandas df 
    Out = int 
    """
    n_vol = data["vlm"].max()
    return n_vol

def filt_vol(data,vol_max):
    """
    In =pandas df , int
    Out = dict 
    """
    sorted_vol = dict()
    for r in range(1,vol_max+1):
        sorted_vol[r] = data[data["vlm"] == r]    
    return sorted_vol

def combine_data_vlm (data,vlm_max,results):
    """
    In =pandas df, int ,dict 
    Out = dict 
    """
    for v_count in range(1,vlm_max+1):
        if v_count not in results:
            results[v_count] = pd.DataFrame()
            #Ensures that the global dataframe exits
        #gathes data objects into list
        frames = [results[v_count],data[v_count]]
                   
        #brings objects in frames into once df
        results[v_count]= pd.concat(frames,axis=0)
    return results


def vlm_analysis_manager(data,extra,results):
    #Master function for volume analysis 
    """
    In  = pd dataframe
    Out = none 
    """
    #Required data 
    presets = ["entry","subentry","vlm"]
    #Combined requried with requested
    
    requests = presets + extra
    #Call the data collie to extract
    r_data = data_collie(data,requests)
    
    #Finds the number of volumes being examined
    n_vol = vol_count(r_data)
    
    #filters by volume to create dict that stores seperate df for each volume 
    filtered_vlm_data = filt_vol(r_data,n_vol)
    
    #results = combine_data_vlm(filtered_vlm_data,n_vol,results)
    return filtered_vlm_data




def volume_analysis(main,add_dat, file_path, stepsize = "0.5mb",
                    ):
    results = dict()
    c = 0
    #loops through the files in small chunks and calls alaysis routines
    for batch, report in main.iterate(step_size=stepsize, report=True,library="pd"):
        dat_batch = batch[0].reset_index()
        results =  vlm_analysis_manager(dat_batch,add_dat,results)
        gc.collect()
        file_name  = "vlm_part_%i.pkl" %c 
        save_pkl(file_path,file_name,results)
        c += 1
    return c

def file_condenser(c,path):
    """Combines pickle files into one master file"""
    rng = np.arange(0,c,1)  
    for c in rng:
        file_name  = "vlm_part_%i.pkl" %c
        m_path = path + file_name
        data srf.load_data(m_path)
        
     
def save_pkl(file_path,file_name,save_results):
    root_name = file_path + file_name
    a_file = open(root_name, "wb")
    pickle.dump(save_results,a_file)
    a_file.close()
   
def execute_threaded(file):
    root_loc = file_path_dicts[file]+file_name_dicts[file]
    root_file = ur.open(root_loc)
    root_file = root_file["t"]
    
    add_dat = ["k","p","stp","pdg","trk"]
    print ("Starting vlm analysis " + file_name_dicts[file])
    count = volume_analysis(root_file,add_dat,file_path_dicts[file],
                                  stepsize="10mb")


    file_condenser(count,file_path_dicts[file])




    gc.collect()

    print("finished "+file_name_dicts[file])
    
    
    
    
#~~~~~~~~~~~~~~~
# Data Loading
#~~~~~~~~~~~~~~~
file_path_dicts = dict()

file_name_dicts=dict ()

#File Production

#Electron Files

# Files Testing
f1_path = "D:\\Gean4_data\\volume_testing\\"
f1_name = "LowerBelt_electron_4l.root"
file_path_dicts["f1"] = f1_path
file_name_dicts["f1"] = f1_name

"""
"f2_path = "D:\\Gean4_data\\al_layers_lower\\2l\\Electrons\\"
f2_name = "LowerBeltElect_2l.root"
file_path_dicts["f2"] = f2_path
file_name_dicts["f2"] = f2_name"
"""
execute_threaded("f1",)
#execute_threaded("f2",)