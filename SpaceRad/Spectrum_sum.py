# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 15:16:12 2021

@author: Andrew
"""

#import SpaceRad_functions as spr
import gc
import uproot as ur


#~~~~~~~~~~~~~~~
# Data Loading
#~~~~~~~~~~~~~~~
root_file = ur.open("D:\\Gean4_data\\Monoenergetic\\Gamma\\Testing\\Gamma_test_v2.root")

root_file = root_file["t"]
spec_df = root_file.arrays(["stp","pdg","k"],library = "pd")

del root_file
gc.collect()
spec_df_or = spec_df["k"][spec_df["stp"] == 0]
photon_k = spec_df_or.unique()
spec_df_Ksum = spec_df["k"][spec_df["stp"] == 0].sum()*1e3*1.60218e-19