# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 09:55:17 2021

@author: Andrew
"""
import uproot as ur
import pandas as pd 
from multiprocessing import Process
import pickle
import gc

        
class engergy_dep():
    def __init__(self):
        self.p_de_t_1 = 0
        self.p_de_t_2 = 0
        
        self.s_de_t_1 = 0
        self.s_de_t_2 = 0
        
    def primary(self,data):
        """Extracts primary particles and sums the deposited energy"""
        p_data_1 = data["de"][(data["trk"] ==  1)&(data["vlm"] ==  1)].sum()*1e3*1.60218e-19
        p_data_2 = data["de"][(data["trk"] ==  1)&(data["vlm"] ==  2)].sum()*1e3*1.60218e-19
        self.p_de_t_1 = self.p_de_t_1 + p_data_1
        self.p_de_t_2 = self.p_de_t_2 + p_data_2
        
    def second(self,data):
        """Extracts secondary particles and sums the deposited energy"""
        s_data_1 = data["de"][(data["trk"] >  1)&(data["vlm"] ==  1)].sum()*1e3*1.60218e-19
        s_data_2 = data["de"][(data["trk"] >  1)&(data["vlm"] ==  2)].sum()*1e3*1.60218e-19
        self.s_de_t_1 = self.s_de_t_1 + s_data_1
        self.s_de_t_2 = self.s_de_t_2 + s_data_2
        
    def dump_data(self, path):
        with  open(path+"DE"+".txt","w") as f :
            f.write("Primary De vlm1: " + str(self.p_de_t_1))
            f.write('\n')
            f.write("Primary De vlm2: " + str(self.p_de_t_2))
            f.write("\n \n")
            f.write("Secondary De vlm1: " + str(self.s_de_t_1))
            f.write('\n')
            f.write("Secondary De vlm2: " + str(self.s_de_t_2))

class process_in_vol():
    def __init__ (self,pdg,vlm):
        self.pdg = pdg
        self.vlm = vlm
        self.process_sec = pd.DataFrame()
        self.process_pri= pd.DataFrame()
    def processes_secondary(self,data):
        """selects particle which are secondaries of the same type on the target.
        It then drops duplicates of the same particle to give a final of all particles that enter
        the volume"""
        p_data = data["pro"][(data["trk"] > 1) &
                      (data["pdg"] == self.pdg) &
                      (data["vlm"] == self.vlm)]
        frames = [self.process_sec, p_data.value_counts()]
        #brings objects in frames into once df
        self.process_sec= pd.concat(frames,axis=1)
        
        #Sums all occurances of the same type of secondary process
        self.process_sec=self.process_sec.sum(axis=1)
        
    def processes_primary(self,data):
        """selects all primary particles that pass through target. see processes_secondary"""
        p_data = data["pro"][(data["trk"] == 1) &
                      (data["pdg"] == self.pdg) &
                      (data["vlm"] == self.vlm)]
        frames = [self.process_pri, p_data.value_counts()]
        #brings objects in frames into once df
        self.process_pri= pd.concat(frames,axis=1)
    
        #Sums all occurances of the same type of primary process
        self.process_pri=self.process_pri.sum(axis=1)
     
    def dump_data(self, path):
        """Outputs finding figues to txt file """
        with  open(path+"process_"+str(self.vlm)+"_particle_"+str(self.pdg)+
                   ".txt","w") as f :
            f.write("Primary processes:" + str(self.process_pri))
            f.write("\n \n")
            f.write("Secondary processes:" + str(self.process_sec))
            
        
class secondary_particleGen_volume:
    def __init__(self,pdg,vlm):
        self.pdg = pdg
        self.vlm = vlm
        self.max = 0 
        self.min = 123e4
        self.spectrum = pd.DataFrame(columns = ["k"])
        self.count =0
        
    def find_particles(self,data):
        self.l_data = data[(data["trk"] > 1) & (data["stp"]==0) &
                      (data["pdg"] == self.pdg) & (data["vlm"] == self.vlm)]
    
    def max_check(self):
        l_max = self.l_data["k"].max()
        if l_max > self.max:
            self.max = l_max
    
    def min_check(self):
        l_min = self.l_data["k"][self.l_data["k"] > 0].min()
        if l_min < self.min:
            self.min = l_min
    
    def clean_l_data(self):
        self.l_data = 0
        
    def particle_counter(self):
        self.count = self.count + self.l_data.shape[0]
    
    def spectrum_combiner(self):
        frames =[self.spectrum,self.l_data["k"]]        
        self.spectrum = pd.concat(frames)
        
    def dump_data(self, path):
        with  open(path+"SecGen_"+str(self.pdg)+"_in_"+str(self.vlm)+
                   "_.txt","w") as f :
            f.write("Max k:" + str(self.max))
            f.write('\n')
            f.write(" Min k:" +str(self.min))
            f.write("\n \n")
            f.write("Particle count: "+str(self.count))
    
    def save_spec(self,path):
        a_file = open(path+"spectrum_of_"+str(self.pdg)+"_in_"+ str(self.vlm)
                      +"_.pkl","wb")
        pickle.dump(self.spectrum,a_file)
        a_file.close()

 
        
 
class ontarget_volume:
    def __init__(self,pdg):
        self.pdg = pdg
        self.max = 0 
        self.min = 123e4
        self.spectrum = pd.DataFrame(columns = ["k"])
        self.count =0
        self.pdg = pdg
        
    def find_particles(self,data):
        self.l_data = data[["vlm","pdg",
                           "entry","trk","k"]][(data["vlm"] == 2
                                           )&
                                           (data["pdg"] == self.pdg)
                           ].drop_duplicates(subset=["entry","trk"])
    
    def max_check(self):
        l_max = self.l_data["k"].max()
        if l_max > self.max:
            self.max = l_max
    
    def min_check(self):
        l_min = self.l_data["k"][self.l_data["k"] > 0].min()
        if l_min < self.min:
            self.min = l_min
        
    def particle_counter(self):
        self.count = self.count + self.l_data.shape[0]
    
    def spectrum_combiner(self):
        frames =[self.spectrum,self.l_data["k"]]        
        self.spectrum = pd.concat(frames)
        
    
    def clean_l_data(self):
        self.l_data = 0
    
    def save_spec(self,path):
        a_file = open(path+"spectrum_onTarget_"+str(self.pdg)
                      +"_.pkl","wb")
        pickle.dump(self.spectrum,a_file)
        a_file.close()
        
    def dump_data(self, path):
        with  open(path+"onTarget_"+str(self.pdg)+
                   "_.txt","w") as f :
            f.write("Max k:" + str(self.max))
            f.write('\n')
            f.write(" Min k:" +str(self.min))
            f.write("\n \n")
            f.write("Particle count: "+str(self.count))

def obj_process(obj,dat_batch):
    obj.find_particles(dat_batch)
    obj.min_check()
    obj.max_check()
    obj.spectrum_combiner()
    obj.particle_counter()
    obj.clean_l_data()
    gc.collect()
    
#Calls for the object outputs to be dumped 
def obj_output(obj,path):
    obj.dump_data(path)
    obj.save_spec(path)
    gc.collect()

#initiates the primary and secondary processing steps
def process_processor(obj,dat_batch):
    obj.processes_secondary(dat_batch)
    obj.processes_primary(dat_batch)
    gc.collect()

def process_output(obj,path):
    obj.dump_data(path)
    gc.collect()
    
def run(file):
    root_loc = file_path_dicts[file]+file_name_dicts[file]
    root_file = ur.open(root_loc)
    root_file = root_file["t"]
    
    electrons_1 =secondary_particleGen_volume(11,1)
    electrons_1_pro_pri =process_in_vol(11,1)
    electrons_1_pro_sec =process_in_vol(11,1)
    
    electrons_2 =secondary_particleGen_volume(11,2)
    electrons_2_pro_pri =process_in_vol(11,2)
    electrons_2_pro_sec =process_in_vol(11,2)
    
    protons_1 = secondary_particleGen_volume(2212,1)
    protons_1_pro_pri =process_in_vol(2212,1)
    protons_1_pro_sec =process_in_vol(2212,1)
    
    protons_2 = secondary_particleGen_volume(2212,2)
    protons_2_pro_pri =process_in_vol(2212,2)
    protons_2_pro_sec =process_in_vol(2212,2)
    
    photons_1 = secondary_particleGen_volume(22,1)
    photons_1_pro_pri =process_in_vol(22,1)
    photons_1_pro_sec =process_in_vol(22,1)
    
    photons_2 = secondary_particleGen_volume(22,2)
    photons_2_pro_pri =process_in_vol(22,2)
    photons_2_pro_sec =process_in_vol(22,2)
    
    protons_target =ontarget_volume(2212)
    photons_target =ontarget_volume(22)
    electrons_target =ontarget_volume(11)
    
    energy_de = engergy_dep()
    
    for batch, report in root_file.iterate(step_size="100mb", report=True,library="pd"):
            dat_batch = batch[0].reset_index()
            
            obj_process(electrons_1,dat_batch)
            process_processor(electrons_1_pro_pri,dat_batch)
            process_processor(electrons_1_pro_sec,dat_batch)
            
            obj_process(electrons_2,dat_batch)
            process_processor(electrons_2_pro_pri,dat_batch)
            process_processor(electrons_2_pro_sec,dat_batch)
            
            obj_process(protons_1,dat_batch)
            process_processor(protons_1_pro_pri,dat_batch)
            process_processor(protons_1_pro_sec,dat_batch)
            
            obj_process(protons_2,dat_batch)
            process_processor(protons_2_pro_pri,dat_batch)
            process_processor(protons_2_pro_sec,dat_batch)
            
            obj_process(photons_1,dat_batch)
            process_processor(photons_1_pro_pri,dat_batch)
            process_processor(photons_1_pro_sec,dat_batch)
            
            obj_process(photons_2,dat_batch)
            process_processor(photons_2_pro_pri,dat_batch)
            process_processor(photons_2_pro_sec,dat_batch)
            
            energy_de.primary(dat_batch)
            energy_de.second(dat_batch)
            
            obj_process(protons_target,dat_batch)
            obj_process(photons_target,dat_batch)
            
            obj_process(electrons_target,dat_batch)
            
            
            
            
            gc.collect()
            
    obj_output(electrons_1,file_path_dicts[file])
    process_output(electrons_1_pro_pri,file_path_dicts[file])
    process_output(electrons_1_pro_sec,file_path_dicts[file])
    
    obj_output(electrons_2,file_path_dicts[file])
    process_output(electrons_2_pro_pri,file_path_dicts[file])
    process_output(electrons_2_pro_sec,file_path_dicts[file])
    
    obj_output(protons_1,file_path_dicts[file])
    process_output(protons_1_pro_pri,file_path_dicts[file])
    process_output(protons_1_pro_sec,file_path_dicts[file])
    
    obj_output(protons_2,file_path_dicts[file])
    process_output(protons_2_pro_pri,file_path_dicts[file])
    process_output(protons_2_pro_sec,file_path_dicts[file])
    
    obj_output(photons_1,file_path_dicts[file])
    obj_output(photons_2,file_path_dicts[file])
    
    process_output(photons_1_pro_pri,file_path_dicts[file])
    process_output(photons_1_pro_sec,file_path_dicts[file])
    
    process_output(photons_2_pro_pri,file_path_dicts[file])
    process_output(photons_2_pro_sec,file_path_dicts[file])
    
    energy_de.dump_data(file_path_dicts[file])
    obj_output(protons_target,file_path_dicts[file])
    obj_output(photons_target, file_path_dicts[file])
    
    obj_output(electrons_target, file_path_dicts[file])
    
    del[electrons_1, electrons_2 ,protons_1,protons_2,photons_1 ,photons_1_pro_pri,
    photons_1_pro_sec ,photons_2, photons_2_pro_pri ,photons_2_pro_sec,electrons_target,
    photons_target,protons_target] 
    gc.collect()
            



file_path_dicts = dict()

file_name_dicts=dict ()




f1_path = "H:\\SpaceRad_Local\\ed_reference\\Lower_Belt\\Reference_gamma_boosted\\gamma_protons\\g1\\"
f1_name = "Gamma1_proton_1l_v3.root"
file_path_dicts["f1"] = f1_path
file_name_dicts["f1"] = f1_name


f2_path = "H:\\SpaceRad_Local\\ed_reference\\Lower_Belt\\Reference_gamma_boosted\\gamma_protons\\g2\\"
f2_name = "Gamma2_proton_1l_v3.root"
file_path_dicts["f2"] = f2_path
file_name_dicts["f2"] = f2_name


"""
f3_path = "D:\\Gean4_data\\ti_layers_lower\\1l\\gamma_boosted\\gamma_electrons\\g2\\"
f3_name = "Gamma2_electron_1l_v3_ti.root"
file_path_dicts["f3"] = f3_path
file_name_dicts["f3"] = f3_name


f4_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ti_layers_lower\\1l\\electron\\"
f4_name = "LowerBeltElect_1l_ti.root"
file_path_dicts["f4"] = f4_path
file_name_dicts["f4"] = f4_name

f5_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ti_layers_lower\\1l\\protons\\"
f5_name = "LowerBeltElect_1l_ti.root"
file_path_dicts["f5"] = f5_path
file_name_dicts["f5"] = f5_name


f6_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ti_layers_lower\\1l\\gamma_boosted\\gamma_electrons\\g1\\"
f6_name = "Gamma1_electron_1l_v3_ti.root"
file_path_dicts["f6"] = f6_path
file_name_dicts["f6"] = f6_name


f7_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ti_layers_lower\\1l\\gamma_boosted\\gamma_electrons\\g2\\"
f7_name = "Gamma2_electron_1l_v3_ti.root"
file_path_dicts["f7"] = f7_path
file_name_dicts["f7"] = f7_name


f8_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ti_layers_lower\\1l\\gamma_boosted\\gamma_protons\\g1\\"
f8_name = "Gamma1_proton_1l_v3.root"
file_path_dicts["f8"] = f8_path
file_name_dicts["f8"] = f8_name


f9_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ti_layers_lower\\1l\\gamma_boosted\\gamma_protons\\g2\\"
f9_name = "Gamma2_proton_1l_v3_ti.root"
file_path_dicts["f9"] = f9_path
file_name_dicts["f9"] = f9_name


f10_path = "Z:\\Andrew\\Thesis_SpaceRad\\Geant4\\ti_layers_lower\\1l\\protons\\"
f10_name = "LowerBeltElect_1l_ti.root"
file_path_dicts["f10"] = f10_path
file_name_dicts["f10"] = f10_name



run("f2")


all_proc = []
for file in file_name_dicts:
        print("Started "+file_name_dicts[file])
        all_proc.append(Process(target=run,
                                args=(file,)))   
if __name__ == '__main__': 
     
    for proc in all_proc: 
        proc.start() 
 
    for proc in all_proc: 
        proc.join() 
"""
#run("f1")
run("f2")