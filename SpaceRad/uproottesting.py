# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 16:21:08 2021

@author: Andrew
"""

import uproot as ur
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt


def appenter(path,dtframe):
    dtframe.csv(path,mode="a",header=False)
    
#~~~~~~~~~~~~~~~
# Data Loading
#~~~~~~~~~~~~~~~
root_file = ur.open("D:\\Gean4_data\\Electron_RefSpec_5m\\LowerBeltElect_0l.root")

#root_file = ur.open("G:\\LowerBeltProt_1l.root")
root_file = root_file["t"]
position_df = root_file.arrays(["x","y","z"],library = "pd")
#Volume Specific data
en_vol = root_file.arrays(["et"],library = "pd")
#Particle Momentum
mom = root_file.arrays(["p"], library="pd")

#track test
trk_p = root_file.arrays(["trk","p"], library="pd")

#Energy test
eng_vol= root_file.arrays(["vlm","de"],library = "pd")


#Secondary Processes 
sec_proc= root_file.arrays(["pro","k","stp"],library = "pd")

#Spectrums
Spec = root_file.arrays(["k","stp"],library = "pd")
spec_proccess = Spec[Spec["stp"] == 0]
zero_k = Spec[(Spec["stp"]==0) & (Spec["k"]==0)]
plt.hist(spec_proccess["k"],1000)
plt.yscale("log")
#~~~~~~~~~~~~~~~
# Data filtering
#~~~~~~~~~~~~~~~

#filter only primary particles
original_p =  trk_p.loc[trk_p["trk"] == 1]


#only primary paths
primary_start_p = original_p.loc[slice(None),
     slice(0,0),:]




#filter primary parths of secondary particles
secondary_p = trk_p.loc[trk_p["trk"] != 1]
#The size of this result is the number of secondary particles created.
uni_index = secondary_p.index.get_level_values('entry').unique()


####
#Energy depositioned per volume 
####
#Sorts the volumes 
eng_vol_sort =  eng_vol.sort_values(by=["vlm"])

# Sums energy per volume 
# UNSURE OF UNITS CURRENTLY
eng_vol_gb = eng_vol_sort.groupby(["vlm"])["de"].agg("sum")



####
#processes creating secondary particles
####

#Selects the secondary particles processes from the simulation
sec_proc = sec_proc[(sec_proc["trk"]>1) & (sec_proc["stp"]==0) ]

# The processes that create secondaries counted 
sec_proc_count =  sec_proc.groupby("pro")["trk"].count()


"""
Find all entries for single partile 
"""
#mom.loc[(0),:]



"""
general query structure 
"""
# finds all particles with between 4 and 700 interactions. 
#mom.loc[slice(None),slice(4,700),:]



#~~~~~~~~~~~~~~~
# Plotting
#~~~~~~~~~~~~~~~
spec = sec_proc
en_1  = en_vol.loc[slice(None),
                   slice(0,0),:].droplevel(1).sort_values(by=["et"])

plt.hist(en_1["et"],100)
plt.yscale("log")