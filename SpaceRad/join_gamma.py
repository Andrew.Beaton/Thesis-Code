
import uproot as ur
import gc


#~~~~~~~~~~~~~~~
# Data Loading
#~~~~~~~~~~~~~~~
gamma1_path = "D:\\Gean4_data\\Monoenergetic\\Gamma\\Electron_Spec_match\\gamma1\\Gamma_test_v3.root"
gamma2_path = "D:\\Gean4_data\\Monoenergetic\\Gamma\\Electron_Spec_match\\gamma2\\Gamma2_test_v3.root"



def array_extract(rootfile):
    data = rootfile.arrays(["stp","pdg","k"],library ="pd")
    return data

def load_root_data(path):
    file = ur.open(path)
    file_arrays = file["t"]
    return file_arrays

def primary_extract(data):
    r_data = data[(data["pdg"] == 22) & (data["stp"]==0)]
    return r_data

def energy_sum(data):
    Esum = data["k"].sum()*1e3*1.60218e-19
    return Esum


gamma1 = load_root_data(gamma1_path)
gamma2 = load_root_data(gamma2_path)

gamma1_array = array_extract(gamma1)
gamma2_array = array_extract(gamma2)
del gamma1,gamma2 
gc.collect()

gamma1_array = primary_extract(gamma1_array)
gamma2_array = primary_extract(gamma2_array)

gamma1_Esum = energy_sum(gamma1_array)
gamma2_Esum = energy_sum(gamma2_array)