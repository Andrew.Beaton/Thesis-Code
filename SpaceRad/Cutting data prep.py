# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 19:43:25 2021

@author: Andrew
"""
import uproot as ur
import pandas as pd 
import pickle
#import concurrent.futures 
from multiprocessing import Process
import gc
def secondary_count (data):
    """
    In = pandas df 
    Out =  pandas df 
    """
    ####
    #processes creating secondary particles
    ####
    #Selects the secondary particles processes from the simulation
    sec_proc = data[(data["trk"]>1) & (data["stp"]==0) ]

    # The processes that create secondaries counted 
    sec_proc_count =  sec_proc.groupby("pro")["trk"].count()
    return sec_proc_count



def secondary_count_master(data,results):
    """
    In = pandas df, dict
    Out =  dict
    """
    #groups the same process generating secondary particles and counts them
    # the counts are summed for all batches
    
    #Ensures that the global dataframe exits
    if "0" not in results:
            results[0] = pd.DataFrame()
    frames = [results[0],data]
            
    #brings objects in frames into once df
    results[0]= pd.concat(frames,axis=1)
    
    #Sums all occurances of the same type of secondary process
    results[0] =results[0].sum(axis=1)
    return results
    

def data_collie(data,request):
    """
    In = root file array list, list 
    Out =  pandas df 
    """
    #Setting up variables
    r_data = pd.DataFrame()
    #build dictionary for results
    col_dat = dict()
    
    #loops for all requests
    for r in request:
        try:
            col_dat[r] = data[r]
        except:
            print("There was an error in data retrieval "+r)
    r_data = pd.DataFrame.from_dict(col_dat)
    

    return r_data
        
def secondary_condition(data,extra,results):
    """
    In = root file array list, list,  dict 
    Out =  dict 
    """
    #calls data required for secondary analyis
    presets = ["pro","pdg","trk","stp"]
    
    #joins esential tags to requested ones
    requests =  presets + extra
    
    #calls data retreval function
    r_data = data_collie(data,requests)
    
    #carries out secondary analysis
    sec_count = secondary_count(r_data)
    results = secondary_count_master(sec_count,results)
    return results

def sum_de_entries(data,vol_max):
    """
    In =dict,  dict 
    Out =  dict 
    """
    res =dict()
    #loops through all volumes
    for r in range(1,vol_max+1):
        
        #sums partile deposited energy across multiple tracks
        res[r] = data[r].groupby(["entry"]).de.sum().reset_index()
    return res
     
def combine_eng_dep(data,vlm_max,results):
    """
    In =pandas df ,  int,dict 
    Out =  dict 
    """
    #loops through all volumes
    for v_count in range(1,vlm_max+1):
        
        #chekcs if the volume results already exist
        if v_count not in results:
            results[v_count] = pd.DataFrame()
            #Ensures that the global dataframe exits
        
        #gathes data objects into list
        frames = [results[v_count],data[v_count]]
                   
        #brings objects in frames into once df
        results[v_count]= pd.concat(frames,axis=0)
    return results
     
def filt_vol(data,vol_max):
    """
    In =pandas df , int
    Out = dict 
    """
    sorted_vol = dict()
    for r in range(1,vol_max+1):
        sorted_vol[r] = data[data["vlm"] == r]    
    return sorted_vol

def vol_count(data):
    """
    In =pandas df 
    Out = int 
    """
    n_vol = data["vlm"].max()
    return n_vol

def combine_data_vlm (data,vlm_max,results):
    """
    In =pandas df, int ,dict 
    Out = dict 
    """
    for v_count in range(1,vlm_max+1):
        if v_count not in results:
            results[v_count] = pd.DataFrame()
            #Ensures that the global dataframe exits
        #gathes data objects into list
        frames = [results[v_count],data[v_count]]
                   
        #brings objects in frames into once df
        results[v_count]= pd.concat(frames,axis=0)
    return results
           
def de_manager(data,extra,results):
    """
    In =pandas df, list ,dict 
    Out = dict 
    """
    #Required data 
    presets = ["entry","pdg","subentry","vlm","de"]
    #Combined requried with requested
    
    requests = presets + extra
    
    
    #Call the data collie to extract
    r_data = data_collie(data,requests)
    
    #Finds the number of volumes being examined
    n_vol = vol_count(r_data)
    
    
    #filters by volume to create dict that stores seperate df for each volume 
    filtered_vlm_data = filt_vol(r_data,n_vol)
    
    
    #Adds the de data to get the energy dep per volume
    filtered_vlm_data_de = sum_de_entries(filtered_vlm_data,n_vol)
    results = combine_eng_dep(filtered_vlm_data_de,n_vol,results)
    return results

def vlm_analysis_manager(data,extra,results):
    #Master function for volume analysis 
    """
    In  = pd dataframe
    Out = none 
    """
    #Required data 
    presets = ["entry","subentry","vlm"]
    #Combined requried with requested
    
    requests = presets + extra
    #Call the data collie to extract
    r_data = data_collie(data,requests)
    
    #Finds the number of volumes being examined
    n_vol = vol_count(r_data)
    #filters by volume to create dict that stores seperate df for each volume 
    filtered_vlm_data = filt_vol(r_data,n_vol)
    
    results = combine_data_vlm(filtered_vlm_data,n_vol,results)
    return results




def volume_analysis(main,add_dat, stepsize = "0.5mb",
                    ):
    results = dict()
    #loops through the files in small chunks and calls alaysis routines
    for batch, report in main.iterate(step_size=stepsize, report=True,library="pd"):
        dat_batch = batch[0].reset_index()
        results =  vlm_analysis_manager(dat_batch,add_dat,results)
        gc.collect()
    return results
        
def de_analysis(main,add_dat, stepsize = "0.5mb",
                    ):
    results = dict()
    #loops through the files in small chunks and calls alaysis routines
    for batch, report in main.iterate(step_size=stepsize, report=True,library="pd"):
        dat_batch = batch[0].reset_index()
        results =  de_manager(dat_batch,add_dat,results)
        gc.collect()
    return results
        

def secondary_analysis(main,add_dat,
         stepsize = "0.5mb"):
    results = dict()
    for batch, report in main.iterate(step_size=stepsize, report=True,library="pd"):
        dat_batch = batch[0].reset_index()
        results = secondary_condition(dat_batch,add_dat,results)
        gc.collect()
    return results
        
 

def save_finals(path,save_results):
    
    for file in range(0,len(save_results)):
          a_file = open(path[file], "wb")
          pickle.dump(save_results[file],a_file)
          a_file.close()
   
def execute_threaded(file):
    root_loc = file_path_dicts[file]+file_name_dicts[file]
    root_file = ur.open(root_loc)
    root_file = root_file["t"]


    add_dat = ["k","stp","pdg","trk"]
    print ("Starting vlm analysis " + file_name_dicts[file])
    vol_results = volume_analysis(root_file,add_dat,stepsize="10mb")


    save_results = []
    save_list =[]
    if "vol_results" in locals():
        save_results.append(vol_results)
        save_list.append(file_path_dicts[file] + "vlm_sec_dict_step.pkl")
        
    save_finals(save_list,save_results)
    save_results = []
    save_list =[]


    gc.collect()

    print("Starting Secondary analysis " + file_name_dicts[file])
    secondary_results  = secondary_analysis(root_file,[],stepsize="10mb")
    

    save_results = []
    save_list =[]
    
    if "secondary_results" in locals():
        save_results.append(secondary_results)
        save_list.append(file_path_dicts[file] + "sec_final.pkl")
        
    save_finals(save_list,save_results)
    
    gc.collect()

    
    print("Starting de analysis " + file_name_dicts[file])
    de_results = de_analysis(root_file,[],stepsize="10mb")

    save_results = []
    save_list =[]
       
    
    if "de_results" in locals():
        save_results.append(de_results)
        save_list.append(file_path_dicts[file] + "vlm_eng_dep.pkl")
    
    
    # Call functions here to save the data    
    save_finals(save_list,save_results)
    gc.collect()
    
    
    """##
    #Save Data 
    ##
    save_results = []
    save_list =[]
    if "vol_results" in locals():
        save_results.append(vol_results)
        save_list.append(file_path_dicts[file] + "vlm_final_dict_step.pkl")
        
    save_finals(save_list,save_results)
    save_results = []
    save_list =[]
    
    if "secondary_results" in locals():
        save_results.append(secondary_results)
        save_list.append(file_path_dicts[file] + "sec_final.pkl")
        
    save_finals(save_list,save_results)
    save_results = []
    save_list =[]
       
    
    if "de_results" in locals():
        save_results.append(de_results)
        save_list.append(file_path_dicts[file] + "vlm_eng_dep.pkl")
    
    
    # Call functions here to save the data    
    save_finals(save_list,save_results)"""



    
    
    print("finished "+file_name_dicts[file])
    
 


#~~~~~~~~~~~~~~~
# Data Loading
#~~~~~~~~~~~~~~~
file_path_dicts = dict()

file_name_dicts=dict ()

#File Production

#Electron Files

# Files Testing

"""
f1_path = "D:\\Gean4_data\\ed_reference\\Lower_Belt\\Reference_gamma_eng_rep\\gamma_electrons\\g1\\"
f1_name = "Gamma1_electron_1l_v3.root"
file_path_dicts["f1"] = f1_path
file_name_dicts["f1"] = f1_name


f2_path = "D:\\Gean4_data\\ed_reference\\Lower_Belt\\Reference_gamma_eng_rep\\gamma_electrons\\g2\\"
f2_name = "Gamma2_electron_1l_v3.root"
file_path_dicts["f2"] = f2_path
file_name_dicts["f2"] = f2_name
"""

f3_path = "H:\\SpaceRad_Local\\ed_reference\\Lower_Belt\\Reference_Protons\\"
f3_name = "LowerBelt_Proton_1l.root"
file_path_dicts["f3"] = f3_path
file_name_dicts["f3"] = f3_name


f4_path = "H:\\SpaceRad_Local\\ed_reference\\Lower_Belt\\Reference_electrons\\"
f4_name = "LowerBeltElect_1l.root"
file_path_dicts["f4"] = f4_path
file_name_dicts["f4"] = f4_name


#execute_threaded("f2",)
#execute_threaded("f3",)
#execute_threaded("f4",)

#File Production

#Proton Files 
"""
f1_path = "D:\\Gean4_data\\al_layers_lower\\1l\\Protons\\"
f1_name = "LowerBelt_Proton_1l.root"
file_path_dicts["f1"] = f1_path
file_name_dicts["f1"] = f1_name

f2_path = "D:\\Gean4_data\\al_layers_lower\\2l\\Protons\\"
f2_name = "LowerBelt_Proton_2l.root"
file_path_dicts["f2"] = f2_path
file_name_dicts["f2"] = f2_name


f3_path = "D:\\Gean4_data\\al_layers_lower\\3l\\Protons\\"
f3_name = "LowerBelt_Proton_3l.root"
file_path_dicts["f3"] = f3_path
file_name_dicts["f3"] = f3_name




f4_path = "D:\\Gean4_data\\al_layers_lower\\4l\\Protons\\"
f4_name = "LowerBelt_Proton_4l.root"
file_path_dicts["f4"] = f4_path
file_name_dicts["f4"] = f4_name
"""

#Gamma Files 
"""
f1_path = "D:\\Gean4_data\\al_layers_lower\\1l\\gamma_electrons\\g1\\"
f1_name = "Gamma1_electron_1l_v3.root"
file_path_dicts["f1"] = f1_path
file_name_dicts["f1"] = f1_name

f2_path = "D:\\Gean4_data\\al_layers_lower\\1l\\gamma_electrons\\g2\\"
f2_name = "Gamma2_electron_1l_v3.root"
file_path_dicts["f2"] = f2_path
file_name_dicts["f2"] = f2_name


f3_path = "D:\\Gean4_data\\al_layers_lower\\1l\\gamma_protons\\g1\\"
f3_name = "Gamma1_proton_1l_v3.root"
file_path_dicts["f3"] = f3_path
file_name_dicts["f3"] = f3_name




f4_path = "D:\\Gean4_data\\al_layers_lower\\1l\\gamma_protons\\g2\\"
f4_name = "Gamma2_proton_1l_v3.root"
file_path_dicts["f4"] = f4_path
file_name_dicts["f4"] = f4_name
"""




all_proc = []
for file in file_name_dicts:
        print("Started "+file_name_dicts[file])
        all_proc.append(Process(target=execute_threaded,
                                args=(file,)))   
if __name__ == '__main__': 
     
    for proc in all_proc: 
        proc.start() 
 
    for proc in all_proc: 
        proc.join() 


"""
# Swap this code in if you just want Multithreading

with concurrent.futures.ThreadPoolExecutor() as executor:
    
    [executor.submit(execute_threaded,file) for file in file_name_dicts]
"""