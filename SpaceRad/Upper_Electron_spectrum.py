# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 14:00:23 2021

@author: Andrew
"""

import pandas as pd 
import matplotlib.pyplot as plt
def setfonts():
    
    plt.rcParams['font.family'] = 'DejaVu Sans'
    plt.rcParams['mathtext.fontset'] = 'custom'
    plt.rcParams['font.sans-serif'] = "DejaVu Sans"
    plt.rcParams['mathtext.cal'] = 'DejaVu Sans'
    plt.rcParams['mathtext.it'] = 'DejaVu Sans:italic'
    plt.rcParams['mathtext.rm'] = 'DejaVu Sans'
    plt.rcParams['text.usetex'] = False
    
setfonts()
path  ="Z:\\Andrew\\Thesis_SpaceRad\\Upper_belt_flux\\Electron_Upper_Spectrum.xlsx "
Outpath = "Z:\\Andrew\\Thesis_2019\\V11\\figures\\SpaceRad\\UpperBelt_Electron_flux.png"
p_data = pd.read_excel(path)
plt.figure(figsize=(8,5),dpi =200)
plt.scatter(p_data["Energy"],p_data["Flux"],c="royalblue")

plt.yscale("log")
plt.xscale("log")
plt.xlabel("Energy " r"($MeV$)")
plt.ylabel("Integral Flux (" r"$cm^2 s^{-1} $"")" )
plt.title("Electron Energy Spectrum - Lower Van Allen Belt")
#plt.savefig(Outpath, format="png")

print("Job Done !")