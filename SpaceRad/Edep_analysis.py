# -*- coding: utf-8 -*-
"""
Created on Mon Jul 12 13:02:38 2021

@author: Andrew
"""

import SpaceRad_functions as spr
import gc

path = "D:\\Gean4_data\\al_layers_lower\\1l\\Protons\\ol\\vlm_eng_dep.pkl"

processed_data = spr.load_data(path)

energy_dep = spr.energydepvols(processed_data)

del processed_data
gc.collect
