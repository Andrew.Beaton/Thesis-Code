import numpy as np
import pandas as pd
import scipy.io
import matplotlib.pyplot as plt 
import matplotlib.ticker 
import thesis_functions as tf


def safe_arange(start, stop, step):
    return step * np.arange(start / step, stop / step)

class simulation_data:
    def __init__(self, path, sheetname):
        self.path = path
        self.sheetname =sheetname
        self.colnames = newnames = {"Timing in pS":"time",
                                    "Pseudo Torch charge in pC":"ptorch",
                                    "Real Laser Torch charge in pC":"rltorch",
                                    "Trojan Horse charge in pC":"trojan"}
    
    def load_data(self):
        """Loads data from excel workboot and sets sheet to raw_data"""
        xls = pd.ExcelFile(self.path)
        self.raw_data = pd.read_excel(xls,self.sheetname)
    
    def data_clean(self):
        """removes na values from the raw data and removes columns with no
        numbers"""
        #self.data_processing = self.raw_data.select_dtypes(include =[np.number])
        self.data_processing = self.raw_data.dropna(how="all", axis="columns")

    
    def rename_cols(self):
        """Renames columns using a preset list """
        self.data_processing.rename(columns=self.colnames,
                                    inplace = True)
    def export_data(self):
        """Pulls processing data at requested time and returns it"""
        self.final_data = self.data_processing
        return self.final_data

def process_sim(path:str, sheet:str):
    """Creates class for data and runs through the standard processing set
    currently returns data as output"""
    fname = simulation_data(path,sheet)
    fname.load_data()
    fname.data_clean()
    fname.rename_cols()
    return fname.export_data()


def setfonts():
    
    plt.rcParams['font.family'] = 'DejaVu Sans'
    plt.rcParams['mathtext.fontset'] = 'custom'
    plt.rcParams['font.sans-serif'] = "DejaVu Sans"
    plt.rcParams['mathtext.cal'] = 'DejaVu Sans'
    plt.rcParams['mathtext.it'] = 'DejaVu Sans:italic'
    plt.rcParams['mathtext.rm'] = 'DejaVu Sans'
    plt.rcParams['text.usetex'] = False

def grouping(data,bins):
    data["cuts"]= pd.cut(data.timing,bins=bins)
    res =data[["charge","cuts"]].groupby("cuts").mean()
    res["max"] = data[["charge","cuts"]].groupby("cuts").max()
    res["std"] = data[["charge","cuts"]].groupby("cuts").std()
    res.columns=["Average Charge", "Peak Charge","Standard Deviation"]

    return res

def melt_sims(data):
    """Combines the sim data into a single column for plotting"""
    return data.melt("time",
                       value_name="charge").dropna().drop("variable",1)

def plot_testing(sim,res,bins):
    fig, ax1, = plt.subplots(2,1, sharex=True)
    #ax2 =ax1.twiny()
    sim.plot(x=["time"], y =["charge"],
             kind ="scatter", color="red",
             ax = ax1[0] )
    ax1[1].scatter(alpha=0.8, x =res["bins_starts"] ,y=res["Average Charge"],
                               color=["orange"])
    #res.plot.scatter(alpha=0.8, x =res.index.values ,y="Peak Charge",
    #                           color=["orange"],ax =ax1[1])
    
    #ax1[1].set_xlim(-2,0.5)
    ax1[0].set_xlim(-2,2)
    #ax2.xaxis.set_major_locator(matplotlib.ticker.FixedLocator(bins))
    

def stackedbar(res,sim,details=dict(),):
    """PLots the bar chart for exp charge and 
    the line scatter of simulation charge"""
    

    fig, ax1, = plt.subplots()
    #ax2 =ax1.twinx()
    #ax2 =ax1.twiny()
    ax1.bar(res["bins_starts"],res["Peak Charge"],color="darkorange",edgecolor = "black", width = 0.1)
    #res.plot(x = "bins_starts", y="Peak Charge",kind="bar",alpha=0.8,
    #                           color=["orange"],ax =ax1)
    ax1.bar(res["bins_starts"],res["Average Charge"],color="royalblue", edgecolor = "black", width = 0.1,
            yerr=res["Standard Deviation"],capsize= 2)
    
    #res.plot(x = "bins_starts", y ="Average Charge",kind="bar",alpha=0.8,
    #                          color=["blue"],ax =ax1,
    #                          capsize=3, yerr=res["Standard Deviation"])
    ax1.scatter(sim["time"],sim["charge"], color = "red", zorder = 2, marker = "x" )
    #sim.plot(x="time",
     #        y ="charge",
      #       kind ="bar",color="red",
       #      ax=ax2)
    
    
    ax1.yaxis.grid(True)
    ax1.set_axisbelow(True)
    ax1.set_xlim(-1.1)
    ax1.set_xlabel(details["xtitle"])
    ax1.set_ylabel(details["ytitle"])
    plt.title(details["title"])

    if details["save"] == True:
        plt.savefig(details["title"]+".png", dpi = 400)
    plt.show()

def prep_sim_bins(data,bins_S):
    data["bins"] = bins_S
    data.set_index("bins",inplace = True)
    data.dropna(axis = "rows", inplace = True)
    data["bins_starts"] = data.index.values
    
    
setfonts()
mat = scipy.io.loadmat('Z:\\Andrew\\Thesis_2019\\E210 Data\\allEnergies_charge_vs_timing.mat')
data_1mj = [mat["Timing1mJ"].transpose(),mat["ChargeOnSpectrometer1mJ"].transpose()]
cols = ["timing","charge"]
data_1mj = np.concatenate((data_1mj[0],data_1mj[1]),axis=1)
exp_1mj = pd.DataFrame(data_1mj,columns =cols )
exp_1mj = exp_1mj.sort_values("timing")


#5mj
data_5mj = [mat["Timing5mJ"].transpose(),mat["ChargeOnSpectrometer5mJ"].transpose()]
cols = ["timing","charge"]
data_5mj = np.concatenate((data_5mj[0],data_5mj[1]),axis=1)
exp_5mj = pd.DataFrame(data_5mj,columns =cols )
exp_5mj = exp_5mj.sort_values("timing")


#05mj
data_05mj = [mat["Timing500muJ"].transpose(),mat["ChargeOnSpectrometer500muJ"].transpose()]
cols = ["timing","charge"]
data_05mj = np.concatenate((data_05mj[0],data_05mj[1]),axis=1)
exp_05mj = pd.DataFrame(data_05mj,columns =cols )
exp_05mj = exp_05mj.sort_values("timing")


####################
# Loading sim data 
####################



path = "Z:\\Andrew\\January2018\\E210\\2021-07-11Raw_Sim_data_wb.xlsx"
sheet = "5mj"
data_5mj_sim = process_sim(path,sheet)

sim_plot_data_5mj = melt_sims(data_5mj_sim)
#data_5mj_sim.melt("time",value_name="charge").dropna().drop("variable",1)

sheet = "1mj"
data_1mj_sim = process_sim(path,sheet)
sim_plot_data_1mj = melt_sims(data_1mj_sim)

sheet = "0_5mj"
data_05mj_sim = process_sim(path,sheet)
sim_plot_data_05mj = melt_sims(data_05mj_sim)


#
bins = [-2,-1.9,-1.8,-1.7,-1.6,-1.5,
        -1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,
        -0.7,-0.6,-0.5,-0.4,-0.3,-0.2,
        -0.1,0,0.1,0.2,0.3,0.4,0.5,0.6
        ,0.7,0.8,0.9,1,1.2,1.3,1.4,1.5
        ,1.6,1.7,1.8,1.9,2]

bins_starts = [-2,-1.9,-1.8,-1.7,-1.6,-1.5,
        -1.4,-1.3,-1.2,-1.1,-1,-0.9,-0.8,
        -0.7,-0.6,-0.5,-0.4,-0.3,-0.2,
        -0.1,0,0.1,0.2,0.3,0.4,0.5,0.6
        ,0.7,0.8,0.9,1,1.2,1.3,1.4,1.5
        ,1.6,1.7,1.8,1.9]


res_5mj = grouping(exp_5mj,bins)
res_05mj= grouping(exp_05mj,bins)
res_1mj = grouping(exp_1mj,bins)


#Moving the bin numbers into their own column
"""
res_5mj["bins_starts"] = bins_starts
res_5mj.set_index("bins_starts",inplace = True)
res_5mj.dropna(axis = "rows", inplace = True)
res_5mj["bins_starts"] = res_5mj.index.values
"""
prep_sim_bins(res_5mj,bins_starts)
prep_sim_bins(res_05mj, bins_starts)
prep_sim_bins(res_1mj, bins_starts)

#Plotting 05mj
_05mj_title = "Experimental and Simulation Injected Charge Comparison - 0.5mJ"
details_05mj ={"xlimit_lower":-0.6,"xlimit_upper":16,
               "ylimit_lower":-10,"Y_steps":10,
               "title":_05mj_title,
               "save":False,
               "xtitle":"Timing (Ps)",
               "ytitle":"Charge(pC)"}
stackedbar(res_05mj,sim_plot_data_05mj,details_05mj)

#plot 1mj 
_1mj_title = "Experimental and Simulation Injected Charge Comparison - 1mJ"
details_05mj ={"xlimit_lower":-0.6,"xlimit_upper":16,
               "ylimit_lower":-10,"Y_steps":10,
               "title":_1mj_title,
               "save":False,
               "xtitle":"Timing (Ps)",
               "ytitle":"Charge(pC)"}
stackedbar(res_1mj,sim_plot_data_1mj,details_05mj)

#Plot 5mj
_5mj_title = "Experimental and Simulation Injected Charge Comparison - 5mJ"
details_5mj  ={"xlimit_lower":-0.6,"xlimit_upper":18,
               "ylimit_lower":-100,"Y_steps":50,
               "title":_5mj_title,
               "save":False,
               "xtitle":"Timing (Ps)",
               "ytitle":"Charge(pC)"}

stackedbar(res_5mj,sim_plot_data_5mj,details_5mj)


##Shift 0.5mj data 
sim_plot_data_05mj_shifted = sim_plot_data_05mj
sim_plot_data_05mj_shifted["time"] = sim_plot_data_05mj_shifted["time"] + 0.2

#Plot 05mj shift 
_05mj_title_shifted = "Experimental and Shifted Simulation Injected Charge - 0.5mJ"
details_05mj_shifted  ={"xlimit_lower":-0.6,"xlimit_upper":18,
               "ylimit_lower":-100,"Y_steps":50,
               "title":_05mj_title_shifted,
               "save":False,
               "xtitle":"Timing (Ps)",
               "ytitle":"Charge(pC)"}

stackedbar(res_05mj,sim_plot_data_05mj,details_05mj_shifted)
#plot_testing(sim_plot_data_5mj,res_5mj,bins_starts)

"""
ax1 = res_5mj.plot(kind="bar",alpha=0.8,stacked=True)

ax1.set_xlabel("Timing (Ps)")
ax1.set_ylabel("Charge (pC)")
pbins =bins[-1:]
ax1.set_xticklabels(pbins,rotation=90);
"""
