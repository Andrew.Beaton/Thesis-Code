# -*- coding: utf-8 -*-
"""
Created on Mon May 17 16:34:52 2021

@author: Andrew
"""

import matplotlib.pyplot as plt
import numpy as np
import math

w0 = 20e-6
n = 1 
wlength = 800e-9

z = np.linspace(-50e-4,50e-4,100)
rayl= (math.pi * w0**2 * n) /wlength

wprop = w0*(np.sqrt(1+(z/rayl)**2))

plt.plot(z,wprop)
plt.plot(z,-wprop)