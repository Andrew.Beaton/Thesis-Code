# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 16:11:55 2021

@author: -
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
al=pd.read_excel("Z:\Andrew\Thesis_SpaceRad\electronData.xlsx",sheetname="ElectronAlu")
si=pd.read_excel("Z:\Andrew\Thesis_SpaceRad\electronData.xlsx",sheetname="electronSilicon")
tis=pd.read_excel("Z:\Andrew\Thesis_SpaceRad\electronData.xlsx",sheetname="electronData")
#al.plot("MeV","Stp",logy=True,logx=True)
xinterval = np.arange(0,50,10) 
fig, ax = plt.subplots(figsize=(10,5))
ax.plot(al["energy"],al["stp"],label="Aluminum")
ax.plot(si["energy"],si["stp"],label="Silicon")
ax.plot(tis["energy"],tis["stp"],label="Tissue Equivalent")
ax.set(xlabel="Energy (MeV)",ylabel="Stopping power (MeV $cm^2$/g)"
       ,title="Stopping power for Electrons")
#ax.set_yscale("log")
ax.set_xscale("log")
ax.set_yticks=(xinterval)
plt.legend(loc="upper right")
plt.savefig("Z:\\Andrew\\Thesis 2019\\V1\\figures\\Electron_stp_graph.png",dpi=fig.dpi)