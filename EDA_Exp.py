# -*- coding: utf-8 -*-
"""
Created on Sun Sep 19 19:53:27 2021

@author: andrewb
"""
import numpy as np
import pandas as pd
import scipy.io

mat = scipy.io.loadmat('Z:\\Andrew\\Thesis_2019\\E210 Data\\allEnergies_charge_vs_timing.mat')
data_1mj = [mat["Timing1mJ"].transpose(),mat["ChargeOnSpectrometer1mJ"].transpose()]
cols = ["timing","charge"]
data_1mj = np.concatenate((data_1mj[0],data_1mj[1]),axis=1)
exp_1mj = pd.DataFrame(data_1mj,columns =cols )
exp_1mj = exp_1mj.sort_values("timing")


#5mj
data_5mj = [mat["Timing5mJ"].transpose(),mat["ChargeOnSpectrometer5mJ"].transpose()]
cols = ["timing","charge"]
data_5mj = np.concatenate((data_5mj[0],data_5mj[1]),axis=1)
exp_5mj = pd.DataFrame(data_5mj,columns =cols )
exp_5mj = exp_5mj.sort_values("timing")


#05mj
data_05mj = [mat["Timing500muJ"].transpose(),mat["ChargeOnSpectrometer500muJ"].transpose()]
cols = ["timing","charge"]
data_05mj = np.concatenate((data_05mj[0],data_05mj[1]),axis=1)
exp_05mj = pd.DataFrame(data_05mj,columns =cols )
exp_05mj = exp_05mj.sort_values("timing")


#torch_5mj = data_5mj
torch_5mj = exp_5mj[ exp_5mj.timing < -0.2]
print(torch_5mj["charge"].describe())
torch_5mj.plot.scatter( x ="timing",y="charge"
    )
