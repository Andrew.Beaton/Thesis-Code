# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 15:49:47 2021

@author: Andrew
"""

import pandas as pd 

def data_saver(data,name):
    savename = "E210_" +name+".pkl"
    data.to_pickle(savename)



d_5 ={"Timing":[-0.05,-0.1,-0.2,-0.4,-0.6,-0.8,-1,-2],
                  "charge":[355.55,358.3,363.1,373.1,372.7,372.8,372.7,372.2]}
df_5mj = pd.DataFrame(data=d_5,dtype="float32")
data_saver(df_5mj,"Torch_5mj")


d_1 ={"Timing":[-0.05,-0.1,-0.2,-0.4,-0.6,-0.8,-1,-2],
                  "charge":[93.6,60.8,46.1,35.2,43.9,42.3,46.4,47.6,36]}
df_1mj = pd.DataFrame(data=d_5,dtype="float32")
data_saver(df_1mj,"Torch_1mj")



d_05 ={"Timing":[-0.05,-0.1,-0.2,-0.4,-0.6,-0.8,-1,-2],
                  "charge":[6.9,0.6,0,0,0,0,0,0]}
df_05mj = pd.DataFrame(data=d_5,dtype="float32")
data_saver(df_05mj,"Torch_05mj")