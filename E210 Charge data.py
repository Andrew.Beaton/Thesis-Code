# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 15:02:12 2020

@author: Andrew
"""

import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt 
import scipy.io

exper_data = scipy.io.loadmat('C:\\Users\\Andrew\\Downloads\\allEnergies_charge_vs_timing.mat')
plt.rcParams['font.sans-serif'] = "DejaVu Sans"
def setfonts():
    
    plt.rcParams['font.family'] = 'DejaVu Sans'
    plt.rcParams['mathtext.fontset'] = 'custom'
    plt.rcParams['font.sans-serif'] = "DejaVu Sans"
    plt.rcParams['mathtext.cal'] = 'DejaVu Sans'
    plt.rcParams['mathtext.it'] = 'DejaVu Sans:italic'
    plt.rcParams['mathtext.rm'] = 'DejaVu Sans'
    plt.rcParams['text.usetex'] = False

def get_axis_limits(ax, scale=.9):
    return ax.get_xlim()[1]*scale, ax.get_ylim()[1]*scale
setfonts()


ChargeData_5mj=pd.read_excel("Z:\\Andrew\\January2018\\E210\\2018-03-21-FinalE210DataForCarl-AB.xlsx",sheet_name="5mj")
ChargeData_1mj=pd.read_excel("Z:\\Andrew\\January2018\\E210\\2018-03-21-FinalE210DataForCarl-AB.xlsx",sheet_name="1mj")
ChargeData_05mj=pd.read_excel("Z:\\Andrew\\January2018\\E210\\2018-03-21-FinalE210DataForCarl-AB.xlsx",sheet_name="0_5mj")
#print "data loaded in pandas "

ChargeData_5mj=ChargeData_5mj.dropna(how='all', axis='columns')
ChargeData_1mj=ChargeData_1mj.dropna(how='all', axis='columns')
ChargeData_05mj=ChargeData_05mj.dropna(how='all', axis='columns')
newnames = {"Timing in pS":"time",
            "Pseudo Torch charge in pC":"ptorch",
            "Real Laser Torch charge in pC":"rltorch",
            "Trojan Horse charge in pC":"trojan"}
ChargeData_5mj.rename(columns=newnames,inplace=True)
ChargeData_1mj.rename(columns=newnames,inplace=True)
ChargeData_05mj.rename(columns=newnames,inplace=True)



plt.rcParams.update({'font.size': 8})

fig, axs =plt.subplots(nrows=3,ncols=1,sharex=True,sharey=True,figsize=(5,9))

ax1=ChargeData_5mj.plot(kind="scatter",x="time" , y="rltorch",color="blue",ax=axs[0])
ax2=ChargeData_05mj.plot(kind="scatter",x="time" , y="rltorch",color="purple",ax=axs[2])
ax3=ChargeData_1mj.plot(kind="scatter",x="time" , y="rltorch",color="red",ax=axs[1])

ax4=ChargeData_5mj.plot(kind="scatter",x="time" , y="trojan",color="blue",ax=axs[0])
ax5=ChargeData_05mj.plot(kind="scatter",x="time" , y="trojan",color="purple",ax=axs[2])
ax6=ChargeData_1mj.plot(kind="scatter",x="time" , y="trojan",color="red",ax=axs[1])


ax7=ChargeData_5mj.plot(kind="scatter",x="time" , y="ptorch",color="blue",ax=axs[0])
ax8=ChargeData_05mj.plot(kind="scatter",x="time" , y="ptorch",color="purple",ax=axs[2])
ax9=ChargeData_1mj.plot(kind="scatter",x="time" , y="ptorch",color="red",ax=axs[1])


#plt.axvline(x=-1,alpha=0.2,color="black") # Was used for marking psuedo torch start 

# Showing the area where blowout is expected to be.
ax1.axvspan(-0.02, 0.33, alpha=0.05, color='slategrey')
ax2.axvspan(-0.02, 0.33, alpha=0.05, color='slategrey')
ax3.axvspan(-0.02, 0.33, alpha=0.05, color='slategrey')


#Label the subplots
ax1.annotate('A', xy=get_axis_limits(ax1,scale=0.7),size=12)
ax2.annotate('C', xy=get_axis_limits(ax2,scale=0.7),size=12)
ax3.annotate('B', xy=get_axis_limits(ax3,scale=0.7),size=12)
start, end = ax4.get_ylim()

axs[0].yaxis.set_ticks(np.arange(0, end, 100))


#plt.xlabel("Time", axes=axs)axs[0].set_title("Injected Charge")
#fig.suptitle('Simulation Injected Charge', fontsize=16)
axs[0].set_title("Injected Charge: 5mJ")
axs[1].set_title("Injected Charge: 1mJ")
axs[2].set_title("Injected Charge: 0.5mJ")
fig.suptitle("Simulation Injected Charge",fontsize=12)
for i in range(0,3):
    axs[i].set_xlabel("Time")
    axs[i].set_ylabel(" Injected Charge (pC)")

plt.tight_layout()
#plt.savefig(r'Z:\Andrew\python\Thesis Code\Simulation_Injected_NOTFINISHED.png', dpi=100)
