# -*- coding: utf-8 -*-
"""
Created on Sun Mar 14 16:39:09 2021

@author: Andrew
"""

import pandas as pd
#import h5py
import numpy as np
import thesis_functions as func
import matplotlib.pyplot as plt
#from matplotlib.ticker import PercentFormatter
from matplotlib import cm
import scipy.constants as const 
plt.rcParams['font.sans-serif'] = "DejaVu Sans"

def namedata (data,names):
    data.columns = names
    return data


def injection_beam_sync(beam,timing):
    #Syncs beam position with injection location
    beam_avg= beam.mean()
    injection_pos = timing*const.c
    co_mov_injection = beam_avg - injection_pos
    return co_mov_injection

def data_loader(filename):
    #Read pickle data from location
    data = pd.read_pickle(filename)
    return data 
    
def load_torch():
    #Loads all torch data 
    torch_mj05 = data_loader("E210_Torch_05mj.pkl")
    torch_mj1 = data_loader("E210_Torch_1mj.pkl")
    torch_mj5 = data_loader("E210_Torch_5mj.pkl")
    return torch_mj05,torch_mj1,torch_mj5

def find0crossing(data):
    #finding the mid point of the data provided
    mid = int(data.shape[0]/(2))
    #Slicing the middile of the dat out
    data =data[mid:mid+1,:]
    # Checks for changes in the sign of the data 
    flips =np.diff(np.sign(data))
    #Moves data into a dataframe
    df_flips  = pd.DataFrame(data = flips)
    #Changes the output of the flips check to boolean data
    bol_data = df_flips.ne(0)
    #sets up a storage space
    cutting_pos = []
    
    for i in (bol_data):
        #loops through the boolean data looking for a true value
        if bol_data.iloc[0,i] ==True:
            #if there is a true value for a fliped sign the record the 
            #index of this entry.
            cutting_pos.append(i)
    return cutting_pos

def zcrossing_t(beam,zcrossing):
    beam_avg= beam.mean()
    delta_t = (beam_avg - zcrossing)/const.c
    return delta_t

def f5attr(location,species,attribute):
    atrributes_caller = func.loadh5_attr(location,species)
    val = atrributes_caller[attribute]
    return val

def zerobeamposition(xdata,offset=10e-6):
    minx= xdata.min()
    zerox =xdata - minx +offset
    return zerox

# Loading data
file_loc = "H:\\20180303_TrojanV4_1_DumpPer_05mJ_0ps_BeamElectrons_210.h5"
BeamUnlens_loc = "Z:\\Andrew\\Thesis_2019\\Sims\\Gaussian Beam\\CleanBeam_E210_BeamElectrons_163.h5"
Efield_loc = "H:\\20180303_TrojanV4_1_DumpPer_05mJ_0ps_ElecMultiField_250.h5"

#Defining future colunn names
col_map = ["x_pos","y_pos","z_pos","x_mom","y_mom","z_mom"]
species = "BeamElectrons"


#Loads data from custom function
beam_dat = func.loadh5(file_loc,species)
beam_dat_unlens = func.loadh5(BeamUnlens_loc, species)

#Names the coumns 
beam_data = namedata(beam_dat,col_map)
beam_dat_unlens = namedata(beam_dat_unlens,col_map)

# Loading in pre processed data on injected charge
mj05 = data_loader("E210_mj05.pkl")
mj1 = data_loader("E210_mj1.pkl")
mj5 = data_loader("E210_mj5.pkl")




#attributes 
atrributes_efield = func.loadh5_attr(Efield_loc,"ElecMultiField")
atrributes_beam = func.loadh5_attr(file_loc,species)
# definitions for the axes

# Loads the electric field data
e_field = func.loadh5_field(Efield_loc,"ElecMultiField")
#Finds central location in the e field
centroid = int(np.shape(e_field)[2]/2)

#Extracting a larger central slice to average

e_field_central_0 = e_field[:,:,centroid-3:centroid+3,0]





#Averaging over the extracted centroid

e_field_central = np.average(e_field_central_0,axis=2)



#Transposing the data to the required shape to plot
e_field_central = np.transpose(e_field_central) 



#scatter_plot(beam_data.x_pos,beam_data.y_pos)

SliceData =e_field_central

#Setting up the figure to be plotted 
fig= plt.figure()
ax = fig.gca()

#Calls function to find the zero crossing ppoint
z_crossing = find0crossing(SliceData[:,120:160])
z_crossing_comove = (120 + z_crossing[0]) * 2e-6


#Finding the end of the window in comoving 
co_mv_end = atrributes_efield["windowEnd"] -atrributes_efield["windowstart"]

#Creates elements of the meshgrid in co moving frame
X = np.linspace(0,co_mv_end,num=251)
Y = np.linspace(atrributes_efield["y_lower"],atrributes_efield["y_upper"],num=167)

#Efield data to be plotted
Z=SliceData
X,Y=np.meshgrid(X,Y)

"""
#Levels to be used for the colours of the Efield
levelVector = np.linspace(np.min(Z), np.max(Z), 1000)
surf = ax.contourf(X,Y,Z, cmap=cm.coolwarm, antialiased=False,levels= levelVector)

#Plotting color bars
cbaxes = fig.add_axes([1.,0.10,0.03,0.8])
cbar = fig.colorbar(surf,cax =cbaxes )
"""
#Creates the zero crossing point line
ax.axvline(z_crossing_comove,c="orange",alpha = 0.75)

#Setting limits
ax.set_xlim(175e-6,500e-6)
ax.set_ylim(-75e-6,75e-6)


#Axis styling
ax.ticklabel_format(axis="x",style="sci",scilimits=(0,0))
ax.ticklabel_format(axis="y",style="sci",scilimits=(0,0))


#Preping beam x pos
#Used to plot only every nth point in the beam 
delStep =10

# Moves the beam into co moving space
co_mov_beam = beam_data.x_pos - atrributes_beam["windowstart"]



unlens_start = f5attr(BeamUnlens_loc,species,"windowstart")
co_mov_beam_unlens = beam_dat_unlens.x_pos - unlens_start

#Used comoving beam 
z_crossing_delta_t = zcrossing_t(co_mov_beam,z_crossing_comove)


#Syncing with injeciton timing
injection_comove = injection_beam_sync(co_mov_beam,mj05["timing"]*1e-12)
"""
#Secondary axis for injection
ax2 =ax.twinx()
ax2.scatter(injection_comove,mj05["charge"])
ax.scatter(co_mov_beam[0::delStep], beam_data.y_pos[0::delStep],alpha=0.002)
"""

#plotting function call 
save_name ="E210_BeamInjection_05mj"
# This function creates a plot that also has injected charge on top of the beam 
#func.e210_beam_scatter_plot_injectedCharge(co_mov_beam, beam_data.y_pos,injection_comove,
#                          mj05["charge"],mj1["charge"],mj5["charge"],False,save_name)

#Beam slice with no injection charge data.
save_name ="E210-BeamSlice"
func.e210_beam_scatter_plot(zerobeamposition(co_mov_beam), beam_data.y_pos,
                           injection_comove,False,save_name,transp=0.05)


save_name ="E210-BeamSlice_beam_dat_unlens"
func.e210_beam_scatter_plot(zerobeamposition(co_mov_beam_unlens), beam_dat_unlens.y_pos,
                            injection_comove,False,save_name,transp=0.05)