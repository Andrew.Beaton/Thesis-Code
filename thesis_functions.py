# -*- coding: utf-8 -*-
"""

@author: Andrew
"""


import numpy as np 
import h5py
#import scipy.constants as const
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter

###########################
def loadh5 (MainFilename,species):
    print ("Start loading %s" %(MainFilename))
    f = h5py.File(MainFilename,'r')
    ParticleData = np.array(f[species], dtype="float32")
    f.close()
    ParticleData = pd.DataFrame(data =ParticleData, dtype ="float32" )
    return ParticleData 

def loadh5_field_slice (MainFilename,nslice):
    print ("Start loading %s" %(MainFilename))
    f = h5py.File(MainFilename,'r')
    FieldData= f[species][:,:,1]
    f.close()
    return FieldData 
    
def loadh5_field(MainFilename,species):
    print ("Start loading %s" %(MainFilename))
    f = h5py.File(MainFilename,'r')
    FieldData= np.array(f[species], dtype="float32")#[:,:,:]
    f.close()
    #FieldData = pd.DataFrame(data =FieldData, dtype ="float32" )
    return FieldData 
    
def loadh5_attr(MainFilename,species):
    print ("Start loading atrributes from %s" %(MainFilename))
    f = h5py.File(MainFilename,'r')
    atr ={}
    y_upper = f["compGridGlobalLimits"].attrs["vsUpperBounds"][1]
    y_lower = f["compGridGlobalLimits"].attrs["vsLowerBounds"][1]
    windowStart = f["compGridGlobalLimits"].attrs["vsLowerBounds"][0]
    windowEnd   = f["compGridGlobalLimits"].attrs["vsUpperBounds"][0]
    f.close()
    atr["windowstart"] = windowStart
    atr["windowEnd"] = windowEnd
    atr["y_upper"] = y_upper
    atr["y_lower"] = y_lower
    return atr
    
def e210_beam_scatter_plot_injectedCharge(pos_x,pos_y,injection_comove,charge_data,charge_data2,charge_data3,save_var,save_name):

   left, width = 0.15, 0.65
   bottom, height = 0.05, 0.65
   spacing = 0.05


   rect_scatter = [left, bottom, width, height]
   rect_histx = [left, bottom + height + spacing, width, 0.2]
   rect_histy = [left + width + spacing, bottom, 0.2, height]
   
   # start with a rectangular Figure
   fig = plt.figure(figsize=(8, 8))

   ax_scatter = plt.axes(rect_scatter)
   ax_scatter.tick_params(direction='in', top=True, right=True)
   
   ax_injection = ax_scatter.twinx()
   inject05 = ax_injection.scatter(injection_comove,charge_data,c="orange",marker = "^",label ="0.5mj")
   inject1 = ax_injection.scatter(injection_comove,charge_data2,c="red",marker = "D",label ="1mj")
   inject5 = ax_injection.scatter(injection_comove,charge_data3,c="purple",marker = "o",label ="5mj")
   ax_injection.legend()
   
   
   ax_histx = plt.axes(rect_histx)
   ax_histx.tick_params(direction='in', labelbottom=False)
   #ax_histy = plt.axes(rect_histy)
   #ax_histy.tick_params(direction='in', labelleft=False)

   # the scatter plot:
   ax_scatter.scatter(pos_x, pos_y,alpha=0.01)


   # now determine nice limits by hand:
   binwidth = 2e-6
   lim_upper_x = (np.abs(pos_x).max() / binwidth) * binwidth
   lim_lower_x = (np.abs(pos_x).min() / binwidth) * binwidth
   lim_upper_y = (np.abs(pos_y).max() / binwidth) * binwidth
   lim_lower_y = ((pos_y).min() / binwidth) * binwidth


   bins = np.arange(lim_lower_x, lim_upper_x + binwidth, binwidth)
   bins_y =np.arange(lim_lower_y,lim_upper_y + binwidth,binwidth)

   ax_histx.hist(pos_x, bins=bins,
              weights=np.ones(len(pos_x)) / len(pos_x))


   ax_histx.yaxis.set_major_formatter(PercentFormatter(1))


   #ax_histy.hist(pos_y, bins=bins_y, orientation='horizontal',weights=np.ones(len(pos_y)) / len(pos_y))
   #ax_histy.yaxis.set_major_formatter(PercentFormatter(1))
   
   #Axis naming 
   ax_histx.set_ylabel("Beam Distribution Percentage ")
   ax_injection.set_ylabel("Injected Charge (pC)")
   
   ax_scatter.set_xlabel("Co-Moving Position (m)")
   ax_scatter.set_ylabel("Y Positon (m)")
   
   fig.suptitle("Injected Charge Level and Relative Drive Beam Position")
   #Axis formatting
   ax_scatter.ticklabel_format(axis="y",style="sci",scilimits=(0,0))
   ax_scatter.ticklabel_format(axis="x",style="sci",scilimits=(0,0))
   ax_injection.spines['right'].set_color('orange')
   ax_injection.tick_params(axis='y', colors='orange')
   ax_injection.yaxis.label.set_color('orange')
   
   if save_var == True :
        plt.savefig(save_name,dpi = 300)
        
   plt.show()
   
def e210_beam_scatter_plot(pos_x,pos_y,injection_comove,save_var,save_name,transp=0.1):

   left, width = 0.15, 0.65
   bottom, height = 0.05, 0.65
   spacing = 0.05


   rect_scatter = [left, bottom, width, height]
   rect_histx = [left, bottom + height + spacing, width, 0.2]
   rect_histy = [left + width + spacing, bottom, 0.2, height]
   
   # start with a rectangular Figure
   fig = plt.figure(figsize=(12, 8))

   ax_scatter = plt.axes(rect_scatter)
   ax_scatter.tick_params(direction='in', top=True, right=True)
   
   
   
   ax_histx = plt.axes(rect_histx)
   ax_histx.tick_params(direction='in', labelbottom=False)
   #ax_histy = plt.axes(rect_histy)
   #ax_histy.tick_params(direction='in', labelleft=False)

   # the scatter plot:
   ax_scatter.scatter(pos_x, pos_y,alpha=transp)


   # now determine nice limits by hand:
   binwidth = 2e-6
   lim_upper_x = (np.abs(pos_x).max() / binwidth) * binwidth
   lim_lower_x = (np.abs(pos_x).min() / binwidth) * binwidth
   lim_upper_y = (np.abs(pos_y).max() / binwidth) * binwidth
   lim_lower_y = ((pos_y).min() / binwidth) * binwidth


   bins = np.arange(lim_lower_x, lim_upper_x + binwidth, binwidth)
   bins_y =np.arange(lim_lower_y,lim_upper_y + binwidth,binwidth)

   ax_histx.hist(pos_x, bins=bins,
              weights=np.ones(len(pos_x)) / len(pos_x))


   ax_histx.yaxis.set_major_formatter(PercentFormatter(1))


   #ax_histy.hist(pos_y, bins=bins_y, orientation='horizontal',weights=np.ones(len(pos_y)) / len(pos_y))
   #ax_histy.yaxis.set_major_formatter(PercentFormatter(1))
   
   #Axis naming 
   ax_histx.set_ylabel("Longitudinal \nBeam Distribution ", fontsize=14)
   ax_histx.tick_params(labelsize = 14)
   
   ax_scatter.set_xlabel("Beam propagation direction(m)", fontsize=14)
   ax_scatter.set_ylabel("Y Positon (m)", fontsize=14)
   
   fig.suptitle("Electron Drive Beam Y-slice", fontsize=16)
   #Axis formatting
   ax_scatter.ticklabel_format(axis="y",style="sci",scilimits=(0,0))
   ax_scatter.ticklabel_format(axis="x",style="sci",scilimits=(0,0))
   ax_scatter.tick_params(labelsize = 14)
   if save_var == True :
        plt.savefig(save_name,dpi = 300,bbox_inches='tight')
        print("File Saved !")
   plt.show()

def safe_arange(start, stop, step):
    return step * np.arange(start / step, stop / step)  