# -*- coding: utf-8 -*-
"""
Created on Sun Aug 29 16:54:49 2021

@author: Andrew
"""
import math
WAVELENGTH = 0.8e-06 
PULSE_ENERGY = 1e-3		# [J]
W0 = 20.0e-06	
TAU   = 64e-15
A0 = 6.61156641e-6 * WAVELENGTH * math.sqrt(PULSE_ENERGY/TAU/W0**2)
I = (A0 / 0.85e-9*( WAVELENGTH/1e-6))**2