Thesis Code
Text size in figures 
	axis titles 14
	eg - ax_histx.set_ylabel("Longitudinal \nBeam Distribution ", fontsize=14)
	
	axis tick size 14
	eg - ax_histx.tick_params(labelsize = 14)
	
	figure title 16
	eg - fig.suptitle("Electron Drive Beam Y-slice", fontsize=16)